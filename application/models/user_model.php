<?php if ( ! defined('BASEPATH')) exit ('No direct script access allowed');
/* filename: user_model.php */

/**
 * Model for accessing and changing most user data
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class User_model extends CI_Model
{
    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function getUser($loginCredentials)
    {
        $userData = new stdClass;

        $qryBind = "SELECT id, email, privileges, actual_name, profile_path FROM users WHERE email = ? AND password = ?";
        $qry1 = $this->db->query($qryBind, array($loginCredentials['email'], $loginCredentials['pass']));

        if($qry1->num_rows() > 0) {
            foreach ($qry1->result() as $row) {
                $obj = new stdClass;

                $level = 0;
                if($row->privileges == 'subscriber') {
                    $level = 1;
                } else if($row->privileges == 'writer') {
                    $level = 2;
                } else if($row->privileges == 'headline_writer') {
                    $level = 3;
                } else if($row->privileges == 'moderator') {
                    $level = 4;
                } else if($row->privileges == 'editor') { 
                    $level = 5;
                } else if($row->privileges == 'admin') {
                    $level = 6;
                }
                
                $obj->level = $level;
                $obj->title = $row->privileges;

                $row->privileges = $obj;
                $userData = $row;
            }
            return $userData;
        } else {
            return false;
        }
    }

    public function getMatchedUsers($account)
    {
        $userData = array();

        $qryBind = "SELECT id, email, privileges, actual_name, register_date FROM users WHERE email = ? OR actual_name = ? OR id = ?";
        $qry1 = $this->db->query($qryBind, array($account, $account, $account));

        if($qry1->num_rows() > 0) {
            foreach ($qry1->result_array() as $row) {
                $userData[] = $row;
            }
            return $userData;
        } else {
            return false;
        }
    }

    public function editUserPermission($params)
    {
        $qryBind = "UPDATE users SET privileges = ? WHERE id = ?";
        $this->db->query($qryBind, array($params['privilege'], $params['pid']));

        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function updateUserLoginDate($userLoginDate)
    {
        $qryBind = "UPDATE users SET last_login = ? WHERE email = ?";
        $qry1 = $this->db->query($qryBind, array($userLoginDate['date'], $userLoginDate['email']));

        if($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }
}

?>
