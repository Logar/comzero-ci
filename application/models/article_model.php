<?php if (! defined('BASEPATH')) exit ('No direct script access allowed');
/* filename: article_model.php */

/**
 * Model for article services
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Article_model extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function addArticle($params)
    {
        if ($params['is_headline'] == 2) {
            $qry = $this->db->query("SELECT * FROM articles_view WHERE is_headline = 2");
            if ($qry->num_rows() > 0) {
                $this->db->query("UPDATE articles SET is_headline=1 WHERE is_headline = 2");
            }
        }

        $qry1 = "INSERT INTO articles
            (author, pid, category, title, slug, banner_image, thumb_image, headline_thumb, youtube_video,
            prelude, content, image_caption, creation_date, modified_date, is_headline)
             VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $this->db->query($qry1, array($params['author'], $this->userSession['pid'], $params['category'], $params['title'], $params['slug'],
            $params['primary_image'], $params['thumb_image'], $params['headline_thumb'], $params['youtube_video'],
            $params['prelude'], $params['content'], $params['image_caption'], $params['creation_date'], $params['creation_date'], $params['is_headline']));

        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function editArticle($params)
    {
        if ($params['is_headline'] == 2) {
            $qry = $this->db->query("SELECT * FROM articles_view WHERE is_headline = 2");
            if ($qry->num_rows() > 0) {
                $this->db->query("UPDATE articles SET is_headline = 1 WHERE is_headline = 2");
            }
        }

        if (isset($params['primary_image'])) {
            $qry1 = "UPDATE articles
                SET category = ?, title = ?, slug = ?, banner_image = ?,
                thumb_image = ?, headline_thumb = ?, youtube_video = ?,
                prelude = ?, content = ?, image_caption = ?, modified_date = ?, is_headline = ? WHERE id = ?";
            $this->db->query($qry1, array($params['category'], $params['title'], $params['slug'],
                $params['primary_image'], $params['thumb_image'], $params['headline_thumb'], $params['youtube_video'],
                $params['prelude'], $params['content'], $params['image_caption'], $params['modified_date'], $params['is_headline'], $params['article_id']));
        } else {
            $qry1 = "UPDATE articles
                SET category = ?, title = ?, slug = ?, youtube_video = ?,
                prelude = ?, content = ?, image_caption = ?, modified_date = ?, is_headline = ? WHERE id = ?";
            $this->db->query($qry1, array($params['category'], $params['title'], $params['slug'],
                $params['youtube_video'], $params['prelude'], $params['content'], $params['image_caption'],
                $params['modified_date'], $params['is_headline'], $params['article_id']));
        }

        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function getUserArticles($pid = null)
    {
        $articles = array();
        $count = 0;

        $pid = (!is_null($pid) ? $pid : $this->userSession['pid']);
        
        $qryBind = "SELECT * FROM articles_view WHERE pid = ? ORDER BY creation_date DESC";
        $qry = $this->db->query($qryBind, array($pid));

        if ($qry->num_rows() > 0) {
            foreach ($qry->result_array() as $row) {

                $articles[] = $row;
                $articles[$count]['article_id'] = $row['id'];
                $articles[$count]['creation_date_full'] = $row['creation_date'];
                $articles[$count]['creation_date_formatted'] = date(
                    "M j, Y g:ia T",
                    strtotime($row['creation_date'])
                );
                $articles[$count]['modified_date_formatted'] = date(
                    "M j, Y g:ia T",
                    strtotime($row['modified_date'])
                );
                $count++;
            }
            return $articles;
        } else {
            return false;
        }
    }

    public function getMatchedArticles($queryData)
    {
        $articles = array();
        $count = 0;

        $regex = '%' . $queryData . '%';
        $qryBind = "SELECT * FROM articles_view WHERE author LIKE ? OR title LIKE ? ORDER BY creation_date DESC";
        $qry = $this->db->query($qryBind, array($regex, $regex));

        if ($qry->num_rows() > 0) {
            foreach ($qry->result_array() as $row) {

                $articles[] = $row;
                $articles[$count]['article_id'] = $row['id'];
                $articles[$count]['creation_date_full'] = $row['creation_date'];
                $articles[$count]['creation_date_formatted'] = date(
                    "M j, Y g:ia T",
                    strtotime($row['creation_date'])
                );
                $articles[$count]['modified_date_formatted'] = date(
                    "M j, Y g:ia T",
                    strtotime($row['modified_date'])
                );

                $count++;
            }
            return $articles;
        } else {
            return false;
        }
    }

    public function getTopCategoryArticle($limit)
    {
        $topCategoryArticles = array();
        $count = 0;

        $qry = $this->db->query("SELECT * FROM
            (SELECT * FROM articles_view WHERE is_headline=0 ORDER BY creation_date DESC) AS a1
            GROUP BY a1.category LIMIT $limit"
        );

        if ($qry->num_rows() > 0) {
            foreach ($qry->result_array() as $row) {

                $topCategoryArticles[] = $row;
                $topCategoryArticles[$count]['article_id'] = $row['id'];
                $topCategoryArticles[$count]['creation_date_full'] = $row['creation_date'];
                $topCategoryArticles[$count]['creation_date_formatted'] = date(
                    "M j, Y g:ia T",
                    strtotime($row['creation_date'])
                );
                $topCategoryArticles[$count]['modified_date_formatted'] = date(
                    "M j, Y g:ia T",
                    strtotime($row['modified_date'])
                );
            }
            return $topCategoryArticles;
        } else {
            return false;
        }
    }

    public function getHeadlineArticle()
    {
        $headlineArticles = array();
        $count = 0;

        $qry = $this->db->query("SELECT * FROM articles_view
            WHERE is_headline = 1
            OR is_headline = 2
            ORDER BY is_headline DESC, creation_date DESC"
        );
        if ($qry->num_rows() > 0) {
            foreach ($qry->result_array() as $row) {
                $headlineArticles[] = $row;
                $headlineArticles[$count]['article_id'] = $row['id'];
                $headlineArticles[$count]['headline_number'] = $row['is_headline'];
                $headlineArticles[$count]['creation_date_full'] = $row['creation_date'];
                $headlineArticles[$count]['creation_date_formatted'] = date(
                    "M j, Y g:ia T",
                    strtotime($row['creation_date'])
                );
                $headlineArticles[$count]['modified_date_formatted'] = date(
                    "M j, Y g:ia T",
                    strtotime($row['modified_date'])
                );

                $count++;
            }

            return $headlineArticles;
        } else {
            return false;
        }
    }

    public function getSingleArticle($articleId)
    {
        $articles = array();

        if ($this->userSession['privilege']->level >= 3) {
            $qryBind = "SELECT * FROM articles_view WHERE id = ?";
            $qry = $this->db->query($qryBind, array($articleId));
        } else {
            $qryBind = "SELECT * FROM articles_view WHERE id = ? AND pid = ?";
            $qry = $this->db->query($qryBind, array($articleId, $this->userSession['pid']));
        }

        if ($qry->num_rows() > 0) {
            $row = $qry->row_array();

            $articles = $row;
            $articles['article_id'] = $row['id'];
            $articles['video_url'] = $row['youtube_video'];
            $articles['creation_date_full'] = $row['creation_date'];
            $articles['creation_date_formatted'] = date(
                "M j, Y g:ia T",
                strtotime($row['creation_date'])
            );
            $articles['modified_date_formatted'] = date(
                "M j, Y g:ia T",
                strtotime($row['modified_date'])
            );

            return $articles;
        } else {
            return false;
        }
    }

    public function getSingleArticleWithSlug($slug)
    {
        $articles = array();

        $qryBind = "SELECT * FROM articles_view WHERE slug = ?";
        $qry = $this->db->query($qryBind, array($slug));

        if ($qry->num_rows() > 0) {
            $row = $qry->row_array();

            $articles = $row;
            $articles['article_id'] = $row['id'];
            $articles['primary_image'] = $row['banner_image'];
            $articles['video_url'] = $row['youtube_video'];
            $articles['creation_date_full'] = $row['creation_date'];
            $articles['creation_date_formatted'] = date(
                "M j, Y g:ia T",
                strtotime($row['creation_date'])
            );
            $articles['modified_date_formatted'] = date(
                "M j, Y g:ia T",
                strtotime($row['modified_date'])
            );

            return $articles;
        } else {
            return false;
        }
    }

    public function getArticleComments($article_id)
    {
        $i = 0;
        $comments = array();

        $qry = $this->db->query("SELECT * FROM comments WHERE article_id = $article_id") or die(mysql_error());
        if ($qry->num_rows() > 0) {
            foreach ($qry->result() as $row) {
                $comments[$i]['id'] = $row->id;
                $comments[$i]['article_id'] = $row->article_id;
                $comments[$i]['author'] = $row->author;
                $comments[$i]['content'] = $row->content;
                $comments[$i]['comment_date'] = date(
                    "M j, Y g:ia T",
                    strtotime($row->comment_date)
                );
                $i++;
            }
            return $comments;
        } else {
            return false;
        }
    }

    public function deleteArticle($id)
    {
        $qry1 = $this->db->query("SELECT * FROM articles_view WHERE id = $id LIMIT 1") or die(mysql_error());

        if ($qry1->num_rows() > 0) {
            foreach ($qry1->result() as $row) {
                unlink($row->banner_image);
                unlink($row->thumb_image);
            }
        }

        $qry2 = $this->db->query("DELETE FROM articles WHERE id = $id") or die(mysql_error());
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    // Gets time for public display
    public function getTime($time, $format)
    {
        $curr_time = gmdate($format, $time);

        return $curr_time;
    }
}
