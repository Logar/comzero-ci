<?php if ( ! defined('BASEPATH')) exit ('No direct script access allowed');
/* filename: user_model.php */

/**
 * Model for accessing and changing most user data
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Fb_user_model extends CI_Model
{
    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function getFbUser($loginCredentials)
    {
        $userData = new stdClass;

        $qryBind = "SELECT id, email, privileges, actual_name, profile_path, user_status, profile_image 
            FROM facebook_users WHERE username = ?";
        $qry1 = $this->db->query($qryBind, array($loginCredentials['username']));

        if($qry1->num_rows() > 0) {
            foreach ($qry1->result() as $row) {
                $obj = new stdClass;

                $level = 0;
                if($row->privileges == 'subscriber') {
                    $level = 1;
                } else if($row->privileges == 'writer') {
                    $level = 2;
                } else if($row->privileges == 'headline_writer') {
                    $level = 3;
                } else if($row->privileges == 'moderator') {
                    $level = 4;
                } else if($row->privileges == 'editor') { 
                    $level = 5;
                } else if($row->privileges == 'admin') {
                    $level = 6;
                }
                
                $obj->level = $level;
                $obj->title = $row->privileges;

                $row->privileges = $obj;
                $userData = $row;
            }
            return $userData;
        } else {
            return false;
        }
    }

    public function updateFbUserLoginDate($userLoginDate)
    {
        $qryBind = "UPDATE facebook_users SET last_login = ? WHERE username = ?";
        $qry1 = $this->db->query($qryBind, array($userLoginDate['date'], $userLoginDate['username']));

        if($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    public function addNewFbUser($params)
    {
        $qryBind = "INSERT INTO facebook_users (actual_name, username, email, profile_path, register_date, last_login)
        VALUES(?, ?, ?, ?, ?, ?)";
        $qry1 = $this->db->query($qryBind, array($params['actual_name'], $params['username'], $params['email'],
        $params['profile_path'], $params['registered'], $params['registered']));

        if($this->db->affected_rows()) {
            return true;
        }
        return false;
    }

}

?>
