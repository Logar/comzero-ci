<?php if ( ! defined('BASEPATH')) exit ('No direct script access allowed');
/* filename: register_model.php */

/**
 * Model for registering users
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Register_model extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function findRegisteredUser($currEmail) {
        $i = 0;
        $arr = array();

        $qryBind = "SELECT * FROM users WHERE email= ?";
        $qry1 = $this->db->query($qryBind, array($currEmail));

        if($qry1->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function addNewUser($params) {

        // query binding
        $qry_bind = "INSERT INTO users (password, actual_name, email, profile_path, register_date, last_login)
        VALUES(?, ?, ?, ?, ?, ?)";
        $qry1 = $this->db->query($qry_bind, array($params['pass'], $params['actual_name'], $params['email'],
        $params['profile_path'], $params['registered'], $params['registered']));

        if($this->db->affected_rows()) {
            return true;
        }
        return false;
    }

    public function setUserPass($newPass, $currUser) {

        // query binding
        $qry_bind = "UPDATE users SET password = ? WHERE email= ? ";
        $qry1 = $this->db->query($qry_bind, array($newPass, $currUser));

        if($this->db->affected_rows()) {
            return true;
        }

        return false;
    }

    public function updatePendingPassReset($currUser) {

        // query binding
        $qry_bind = "UPDATE `reset_passwords` SET `reset_done` = 1 WHERE `username`= ? ";
        $qry1 = $this->db->query($qry_bind, array($currUser));

        if($this->db->affected_rows()) {
            return true;
        }

        return false;
    }

    public function addPendingPassReset($currUser, $auth) {

        // query binding
        $qry_bind = "INSERT INTO `reset_passwords` (`email`, `auth`) VALUES(?, ?)";
        $qry1 = $this->db->query($qry_bind, array($currUser, $auth));

        if($this->db->affected_rows()) {
            return true;
        }

        return false;
    }

    public function findPendingPassReset($currUser, $auth) {

        // query binding
        $qry_bind = "SELECT * FROM reset_passwords WHERE email = ? AND auth= ? AND reset_done = 0";
        $qry1 = $this->db->query($qry_bind, array($currUser, $auth));

        if($qry1->num_rows() > 0) {
            return true;
        }

        return false;
    }
}

?>
