<?php if ( ! defined('BASEPATH')) exit ('No direct script access allowed');
/* filename: admin_model.php */

/**
 * Model for admin services
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Admin_model extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function getUsers() {

        $users = array();

        $qry = $this->db->query(
            "SELECT id AS pid, email, actual_name, privileges FROM users
            ORDER BY privileges ASC");
        if($qry->num_rows() > 0) {
            foreach ($qry->result_array() as $row) {
                $users[] = $row;
            }
            return $users;
        } else {
            return false;
        }
    }

    public function getNewsAlert($searchQuery) {

        $alerts = array();

        $qryBind = "
            SELECT * FROM news_alerts
            WHERE source = ? OR message = ?
            ";
        $qry = $this->db->query($qryBind, array($searchQuery, $searchQuery));
        if($qry->num_rows() > 0) {
            foreach ($qry->result_array() as $row) {
                $alerts[] = $row;
            }
            return $alerts;
        } else {
            return false;
        }
    }

    public function getMatchedAlert($searchQuery) {

        $alerts = array();

        $regex = '%' . $searchQuery . '%';
        $qryBind = "
            SELECT * FROM news_alerts
            WHERE author LIKE ? OR source LIKE ? OR message LIKE ?
            ";
        $qry = $this->db->query($qryBind, array($regex, $regex, $regex));
        if($qry->num_rows() > 0) {
            foreach ($qry->result_array() as $row) {
                $alerts[] = $row;
            }
            return $alerts;
        } else {
            return false;
        }
    }

    public function getSingleNewsAlert($alertId) {

        $alerts = array();

        $qryBind = "
            SELECT * FROM news_alerts
            WHERE id = ?
            ";
        $qry = $this->db->query($qryBind, array($alertId));
        if($qry->num_rows() > 0) {
            foreach ($qry->result_array() as $row) {
                $alerts = $row;
            }
            return $alerts;
        } else {
            return false;
        }
    }

    public function createAlert($alert) {

        $qryBind = "INSERT INTO news_alerts (author, source, message, alert_date) VALUES(?, ?, ?, ?)";
        $qry = $this->db->query($qryBind, array(
            $this->userSession['member'],
            $alert['source'],
            $alert['message'],
            $alert['date']
        ));

        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function editAlert($alert) {

        $qryBind = "UPDATE news_alerts SET source = ?, message = ?, alert_date = ? WHERE id = ?";
        $qry = $this->db->query($qryBind, array(
            $alert['source'],
            $alert['message'],
            $alert['date'],
            $alert['id']
        ));

        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteComment($id) {

        $i = 0;
        $arr = array();

        $qry = $this->db->query("DELETE FROM `comments` WHERE `id`=$id") or die(mysql_error());
        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }
}

?>
