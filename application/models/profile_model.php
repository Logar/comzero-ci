<?php if (! defined('BASEPATH')) exit ('No direct script access allowed');
/* filename: profile_model.php */

/**
 * Model for profile services
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Profile_model extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function getUserProfile($pid)
    {
        $profileData = array();
        $count = 0;

        $qryBind = "SELECT id, biography FROM profiles WHERE pid = ?";
        $qry = $this->db->query($qryBind, array($pid));

        if ($qry->num_rows() > 0) {
            $row = $qry->row(); 
            return $row;
        } else {
            return false;
        }
    }

    public function getUser($profilePath)
    {
        $userData = new stdClass;

        $qryBind = "SELECT id, email, privileges, actual_name, profile_path, profile_image FROM users_view 
            WHERE profile_path = ?";
        $qry1 = $this->db->query($qryBind, array($profilePath));

        if($qry1->num_rows() > 0) {
            foreach ($qry1->result() as $row) {
                $obj = new stdClass;

                $level = 0;
                if($row->privileges == 'subscriber') {
                    $level = 1;
                } else if($row->privileges == 'writer') {
                    $level = 2;
                } else if($row->privileges == 'headline_writer') {
                    $level = 3;
                } else if($row->privileges == 'moderator') {
                    $level = 4;
                } else if($row->privileges == 'editor') { 
                    $level = 5;
                } else if($row->privileges == 'admin') {
                    $level = 6;
                }
                
                $obj->level = $level;
                $obj->title = $row->privileges;

                $row->privileges = $obj;
                $userData = $row;
            }
            return $userData;
        } else {
            return false;
        }
    }

    public function updateProfile($profile)
    {
        $qry1 = "SELECT * FROM profiles WHERE pid = ?";
        $results1 = $this->db->query($qry1, array($profile['pid']));

        if ($results1->num_rows() > 0) {
            $qry2 = "UPDATE profiles SET biography = ? WHERE pid = ?";
            $this->db->query($qry2, array($profile['biography'], $profile['pid']));
        } else {
            $qry2 = "INSERT INTO profiles (pid, biography) VALUES (?, ?)";
            $this->db->query($qry2, array($profile['pid'], $profile['biography']));
        }

        return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function getUserArticles($pid)
    {
        $articles = array();
        $count = 0;

        $qryBind = "SELECT * FROM articles_view WHERE pid = ? ORDER BY creation_date DESC";
        $qry = $this->db->query($qryBind, array($pid));

        if ($qry->num_rows() > 0) {
            foreach ($qry->result_array() as $row) {

                $articles[] = $row;
                $articles[$count]['article_id'] = $row['id'];

                $count++;
            }
            return $articles;
        } else {
            return false;
        }
    }

    public function getUserStatsCount($pid)
    {
        $qryBind1 = "SELECT SUM(youtube_video <> '') AS video_count,
            COUNT(*) AS article_count
            FROM articles WHERE pid = ?";
        $qry1 = $this->db->query($qryBind1, array($pid));

        if ($qry1->num_rows() > 0) {
            return $qry1->row(); 
        } else {
            return false;
        }
    }

    // Gets time for public display
    public function getTime($time, $format)
    {
        $curr_time = gmdate($format, $time);

        return $curr_time;
    }
}
