<?php if ( ! defined('BASEPATH')) exit ('No direct script access allowed');
/* filename: general_model.php */

/**
 * Model for general functionality and services
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class General_model extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function getCategoryPage($category)
    {
        $arr = array();
        $i = 0;

        $qry = $this->db->query("SELECT * FROM articles_view WHERE category = '$category' ORDER BY creation_date DESC") or die(mysql_error());
        if($qry->num_rows() > 0) {
            foreach ($qry->result() as $row)
            {
                $arr[$i]['article_id'] = $row->id;
                $arr[$i]['author'] = $row->author;
                $arr[$i]['profile_path'] = $row->profile_path;
                $arr[$i]['title'] = $row->title;
                $arr[$i]['slug'] = $row->slug;
                $arr[$i]['thumb_image'] = $row->thumb_image;
                $arr[$i]['prelude'] = $row->prelude;
                $arr[$i]['content'] = $row->content;
                $arr[$i]['category'] = $row->category;
                $arr[$i]['creation_date_full'] = $row->creation_date;
                $arr[$i]['creation_date_formatted'] = $this->getTime(
                    strtotime($row->creation_date),
                    "D M j, Y g:ia T"
                );
                $i++;
            }
            return $arr;
        } else {
            return false;
        }
    }

    // From main_model.php
    public function getStoredPage($page)
    {
        $qry = $this->db->query("SELECT * FROM pages WHERE page_name = '$page'") or die(mysql_error());
        if($qry->num_rows() > 0) {
            $arr = $qry->row_array();
            return $arr;
        }
        return false;
    }

    // From main_model.php
    public function getVideos($limit)
    {
        $i = 0;
        $qry = $this->db->query("SELECT youtube_video FROM articles_view WHERE youtube_video != '' ORDER BY modified_date DESC LIMIT $limit") or die(mysql_error());
        if($qry->num_rows() > 0) {
            foreach ($qry->result() as $row)
            {
                $arr[$i]['youtube_video'] = $row->youtube_video;

                $i++;
            }
            return $arr;
        } else {
            return false;
        }
    }

    public function globalSearch($queryData)
    {
        $searchResults = array();
        $regex = '%' . $queryData . '%';

        $qryBind1 = "SELECT email, actual_name AS fullname, profile_image, profile_path, privileges FROM users
            WHERE email LIKE ?
            OR actual_name LIKE ?
            OR profile_path LIKE ?";
        $qry1 = $this->db->query($qryBind1, array($regex, $regex, $regex));

        $qryBind2 = "SELECT author, title, slug, prelude, thumb_image, 
            category, profile_path, privileges 
            FROM articles_view
            WHERE author LIKE ? 
            OR slug LIKE ?
            OR title LIKE ?
            ORDER BY modified_date DESC";
        $qry2 = $this->db->query($qryBind2, array($regex, $regex, $regex));

        if ($qry1->num_rows() > 0) {
            $counter = 0;
            foreach ($qry1->result() as $row) {
                $searchResults[] = $row;
                $searchResults[$counter]->profile_image = Utilities::setProfileImage($row->profile_image);
                $searchResults[$counter]->userTitle = Utilities::setUserTitle($row->privileges);
                unset($searchResults[$counter]->privileges);
                $counter++;
            }
        }

        if ($qry2->num_rows() > 0) {
            $counter = 0;
            foreach ($qry2->result() as $row) {
                $searchResults[] = $row;
                $searchResults[$counter]->userTitle = Utilities::setUserTitle($row->privileges);
                unset($searchResults[$counter]->privileges);
                $counter++;
            }
        }

        if (!empty($searchResults)) {
            return $searchResults;
        } else {
            return false;
        }
    }

    public function deleteComment($id)
    {
        $qry = $this->db->query("DELETE FROM comments WHERE id = $id") or die(mysql_error());
        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getBreakingNews()
    {
        $results = array();
        $i = 0;

        $qryBind = "SELECT id, author, message, alert_date
            FROM news_alerts
            ORDER BY alert_date DESC LIMIT 5";
        $qry1 = $this->db->query($qryBind);

        if($qry1->num_rows() > 0) {
            foreach ($qry1->result_array() as $row) {
                $results[] = $row;
            }
            return $results;
        } else {
            return false;
        }
    }

    public function createComment($commentData)
    {
        $qryBind = "INSERT INTO comments (article_id, author, content, comment_date) VALUES(?, ?, ?, ?)";
        $this->db->query(
            $qryBind,
            array(
                $commentData['articleId'],
                $commentData['author'],
                $commentData['content'],
                $commentData['currentDate']
            )
        );
        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteAlert($id)
    {
        $qry = $this->db->query("DELETE FROM news_alerts WHERE id = $id") or die(mysql_error());
        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function addEmailAddress($emailAddress) {

        $qryBind = "INSERT INTO mail_list (email) VALUES (?)";
        $this->db->query($qryBind, $emailAddress);

        if($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    // Gets Eastern time for public display
    public function getTime($utc_time, $format)
    {
        $easternHours = 4;
        $curr_time = gmdate($format, $utc_time - 3600 * $easternHours);

        return $curr_time;
    }
}

?>
