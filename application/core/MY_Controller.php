<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is the base class for all of the controllers. All controllers must extend this class.
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
abstract class MY_Controller extends CI_Controller
{
    /**
    * Stores locally accessed data
    * @var string array
    */
    public $data;

    /**
     * Contains active member's username and pid
     * @var mixed array
     */
    public $userSession;

    /**
     * Object for testing runtime of functions
     * @var object
     */
    public $timer;

    /**
     * Privilege for user
     * @var integer
     */
    public $privilege;

    public function __construct() {

        // Call the CI_Controller constructor
        parent::__construct();

        $this->data = array();
        $this->userSession = $this->session->all_userdata();

        if(!isset($this->userSession['member']) || empty($this->userSession['member'])) {
            $this->userSession['member'] = false;
        }

        if(!isset($this->userSession['pid']) || empty($this->userSession['pid'])) {
            $this->userSession['pid'] = false;
        }

        $this->data['session'] = $this->userSession;

        /* @todo remove or use values
        $auth0 = array("admin"=>"Administrator");
        $auth1 = array("editor"=>"Editor", "publisher"=>"Publisher");
        $auth2 = array("headline_writer"=>"Headline Writer", "writer"=>"Writer");
        */

        if(!isset($this->userSession['privilege'])) {
            $this->privilege = 0;
        }
        
        $this->data['admin_bar'] = $this->load->view('admin/admin_bar', $this->data, true);

        // Initialize these arrays here first
        $this->data['css'] = array();
        $this->data['scripts'] = array();
        $this->data['inline_scripts'] = '';

        $this->data['page_title'] = 'Community Zero';

        $this->timer = '';
    }

    public function getSidebarDependencies() {
        
        $this->data['css'] = array_merge(
            $this->data['css'],
            array(
                '/assets/css/vendor/calendar/datepicker.default.css'
            )
        );
        
        $this->data['scripts'] = array_merge(
            $this->data['scripts'],
            array(
                '/assets/scripts/vendor/jquery-ui/1.8.9/jquery-ui.min.js'
            )
        );

        $this->data['inline_scripts'] = $this->data['inline_scripts'] . 
            "$(function() {
                $('#datepicker').datepicker();
            });";
    }
 }

/**
 * Calculates how long a block of functions run
 *
 * Usage Details
 * $timer = new FuncTimer();
 * $start = $timer->getTime();
 * // Run some functions and return
 * $end = $timer->getTime();
 * echo $timer->getTotalTime($start, $end);
 * exit;
 */
class FuncTimer extends MY_Controller {

    /**
     * Stores start time in microseconds
     * @var float
     */
    public $start;

    /**
     * Stores end time in microseconds
     * @var float
     */
    public $end;

    public function __construct() {
        parent::__construct();

        $this->start = 0;
        $this->end = 0;
    }

    public function getStartTime() {
        $timer = explode(' ', microtime());
        $this->start = $timer[1] + $timer[0];
        return $this->start;
    }

    public function getEndTime() {
        $timer = explode(' ', microtime());
        $this->end = $timer[1] + $timer[0];
        return $this->end;
    }

    public function getTotalTime($start, $end) {
        return round($end - $start, 4).' seconds';
    }
}

class Utilities {

    public static function setUserTitle($title) {

        if ($title == 'admin') {
            $title = 'Editor';
        } else if ($title == 'headline_writer') {
            $title = 'Headline Writer';
        }
        
        return $title;
    }

    public static function setProfileImage($image, $size = 'medium') {

        if (empty($image) && $size = 'medium') {
            $image = DEFAULT_PROFILE_IMAGE_MEDIUM;
        }
        
        return $image;
    }
}

/* End of file  */
/* Location: ./application/core/ */
