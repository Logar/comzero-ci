<?php

function get_snippet($str, $wordCount) {
    return implode(
        '',
        array_slice(
                preg_split(
                        '/([\s,\.;\?\!]+)/',
                        $str,
                        $wordCount*2+1,
                        PREG_SPLIT_DELIM_CAPTURE
                ),
                0,
                $wordCount*2-1
        )
    );
}

function truncateText($string, $limit, $break = '.', $pad = '...', $strict = false) {

    // If the $string is shorter than the $limit, return the original source
    if (strlen($string) <= $limit) {
        return $string;
    }

    // If the string MUST be shorter than the $limit set.
    // Otherwise shorten to the first $break after the $limit
    if ($strict){
        $string = substr($string, 0, $limit);

        if (($breakpoint = strrpos($string, $break)) !== false){
            $string = substr($string, 0, $breakpoint).$pad;
        }
    }
    else{
        // If $break is present between $limit and the end of the string
        if (($breakpoint = strpos($string, $break, $limit)) !== false){
            if ($breakpoint < strlen($string) - 1)
            {
                $string = substr($string, 0, $breakpoint).$pad;
            }
        }
    }

    return $string;
}

// close opened html tags
function closetags($html)
{
    #put all opened tags into an array
    preg_match_all("#<([a-z]+)( .*)?(?!/)>#iU", $html, $result);
    $openedtags = $result[1];

    #put all closed tags into an array
    preg_match_all("#</([a-z]+)>#iU", $html, $result);
    $closedtags = $result[1];
    $len_opened = count($openedtags);

    # all tags are closed
    if(count($closedtags) == $len_opened) {
        return $html;
    }
    $openedtags = array_reverse($openedtags);
    # close tags
    for($i = 0; $i < $len_opened; $i++) {
        if (!in_array($openedtags[$i], $closedtags)) {
            $html .= "</" . $openedtags[$i] . ">";
        }
        else {
            unset ( $closedtags[array_search ( $openedtags[$i], $closedtags)] );
        }
    }
        return $html;
    }
    // close opened html tags
?>