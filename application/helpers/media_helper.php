<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function media_url($uri = '') 
{
    $CI =& get_instance();
    $CI->config->load();
    return $CI->config->item('media_url') . $uri;     
}