<?php

require_once('actions/helpers/RegisterHelper.php');

/**
 * Facebook Login
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class TwitterLogin extends MY_Controller
{
    private $_twtr;
    
    public function __construct()
    {
        // Call the parent constructor
        parent::__construct();

        // Include configuration file
        $this->config->load('twitter');
        // Load the Twitter Auth
        $this->load->library('twitter/twitteroauth');

        $this->load->model('twitter_user_model', 'twitter_user_model');
    }

    public function setTwtrSession()
    {
        // Create a new twitter authentication
        $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
        $requestToken = $connection->getRequestToken(OAUTH_CALLBACK);

        $twitterAuth = array(
            'twtrToken' => $requestToken['oauth_token'],
            'twtrTokenSecret' => $requestToken['oauth_token_secret']
        );
        //$this->session->set_userdata($twitterSession);

        // Continue only if http code is 200
        if ($connection->http_code == '200') {
            // Redirect user to twitter
            //$twitterUrl = $this->_connection->getAuthorizeURL($requestToken['oauth_token']);
            //header('Location: ' . $twitterUrl);
            $this->_getTwtrUser($twitterAuth);
        } else {
            die("We were unable to connect you to twitter. Please try again later.");
        }
    }
    
    protected function _getTwtrUser($twitterAuth)
    {
        if (isset($_REQUEST['oauth_token']) && $twitterAuth['twtrToken']  !== $_REQUEST['oauth_token']) {

            // If token is old, destroy any session and redirect user to login page
            header('Location: ./index.php');
            
        } else if (isset($_REQUEST['oauth_token']) && $twitterAuth['twtrToken'] == $_REQUEST['oauth_token']) {

            // Everything looks good, request access token
            // Successful response returns oauth_token, oauth_token_secret, user_id, and screen_name
            $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $twitterAuth['twtrToken'], $twitterAuth['twtrTokenSecret']);
            $accessToken = $connection->getAccessToken($_REQUEST['oauth_verifier']);
            if($connection->http_code === '200') {
                $_SESSION['status'] = 'verified';
                $_SESSION['request_vars'] = $accessToken;

                // Check user id in our database 
                $userData = $this->twitter_user_model->getTwtrUser($twtrUser);
                
                if($userData !== false) {   
                    // User exists
                    // Set user session variables
                    $this->setUserSession($userData);

                    // Redirect to the user's profile
                    header('Location: ' . SITE_URL . $userData->profile_path);
                } else {

                    // User is new, therefore insert a new user entry
                    // Insert user into Database
                    $this->_registerTwtrUser($twtrUser);
                    $userData = $this->twitter_user_model->getTwtrUser($twtrUser);
                    // Set user session variables
                    $this->setUserSession($userData);
                    
                    // Redirect to the user's profile
                    header('Location: ' . SITE_URL . $userData->profile_path);
                }
                
            } else {
                die("We were unable to connect you to twitter. Please try again later.");
            }
                
        }
    }

    private function _registerTwtrUser($twtrUser) 
    {
        $date = date('Y-m-d H:i:s');
        $registerData['registered'] = $date;
        $registerData['actual_name'] = $twtrUser['fullname'];
        $registerData['profile_path'] = RegisterHelper::crc64($twtrUser['email'] . time(), '%u');
        $registerData['status'] = 0;

        $this->twitter_user_model->addNewTwtrUser($registerData);
    }
    
    public function setUserSession($userData)
    {
        if ($userData !== false) {
            if ($userData->status == 2) {
                header('Location: /login/?login_attempt=2');
            } else {
                $userLoginDate['email'] = $userData->email;
                $userLoginDate['date'] = date('Y-m-d H:i:s');
                $this->fb_user_model->updateTwtrUserLoginDate($userLoginDate);

                $newUserArray = array(
                    'pid'           => $userData->id,
                    'member'        => $userData->email,
                    'privilege'     => $userData->privileges,
                    'fullname'      => $userData->actual_name,
                    'profile_path'  => $userData->profile_path,
                    'profile_image' => $userData->profile_image
                );

                $this->session->set_userdata($newUserArray);

                return true;
            }
        }
        else {
            return false;
        }
    }
}

?>
