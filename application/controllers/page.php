<?php
/* filename: page.php */

/* Include the Template class */
include_once('template.php');

/**
 * Page controller
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Page extends MY_Controller
{
    private $_template;

    public function __construct() {

        parent::__construct();

        // Call the template class
        $this->_template = Template::getTemplateInstance($this);

        $this->load->model('general_model');
    }

    public function determineCategory() {

        $currentPage = $this->uri->segment(1);
        $this->data['page'] = ucwords($currentPage);
        $this->data['articles'] = $this->general_model->getCategoryPage($currentPage);

        $this->getSidebarDependencies();

        $this->_template->generalTemplate(
            $this->_template->initTemplate("article/category", "two_cols", $this->data)
        );
    }

    public function determinePage() {

        $currentPage = $this->uri->segment(1);
        $this->data['page'] = ucwords($currentPage);

        if ($currentPage == 'forgot') {
            $this->data['newsAlerts'] = false;
        }

        $this->getSidebarDependencies();

        $this->_template->generalTemplate(
            $this->_template->initTemplate("generic/" . $currentPage . ".phtml", "two_cols", $this->data)
        );
    }
}

?>