<?php
/* filename: errors.php */

/* Include the Template class */
include_once('template.php');

/**
 * Default controller for displaying page errors
 *
 * @author     Ross Arena
 * @copyright  2012-2013 University of Central Florida
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Errors extends MY_Controller
{
    private $_template;

    public function __construct() {

        // Call the base class constructor
        parent::__construct();

        // Call the template class
        $this->_template = Template::getTemplateInstance($this);

    }

    public function index() {

        $this->data['page_title'] = 'Page Not Found';
        $page = "page_missing";

        $this->_template->generalTemplate(
            $this->_template->initTemplate($page, "one_col", $this->data)
        );
    }
}
?>
