<?php

require_once('actions/helpers/RegisterHelper.php');

/**
 * Facebook Login
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class FbLogin extends MY_Controller
{
    private $_fb;
    
    public function __construct()
    {
        // Call the parent constructor
        parent::__construct();

        // Include configuration file
        /*
        $this->config->load('facebook');
        // Load the FB SDK
        $this->load->library('fb/facebook');

        //initialize facebook sdk
        $this->_fb = new Facebook(array(
            'appId' => $appId,
            'secret' => $appSecret,
        ));
        */
        $this->load->model('user_model', 'user_model');
        $this->load->model('fb_user_model', 'fb_user_model');
    }

    public function getFbUser()
    {
        /*
        $fbUserAuthenticated = $this->_fb->getUser();
    
        if ($fbUserAuthenticated) {
            try {
                // Proceed knowing you have a logged in user who's authenticated
                $fbUser = $this->_fb->api('/me');
                $fbId = $this->_fb->getUser();
            }
            catch (FacebookApiException $e) {
                $fbUserAuthenticated = null;
            }
        }
        
        // Redirect user to facebook login page if empty data or fresh login requires
        if (!$fbUserAuthenticated) {
            $loginUrl = $this->_fb->getLoginUrl(array('redirect_uri'=>$returnUrl, false));
            header('Location: ' . $loginUrl);
        }
        */
        $fbUser['fullname'] = $_POST['fullname'];
        $fbUser['username'] = $_POST['username'];
        //  @todo once fb starts to return an email set this to email instead
        $fbUser['email'] = $_POST['username'];

        // Check user id in our database 
        $userData = $this->fb_user_model->getFbUser($fbUser);
        
        if($userData !== false) {   
            // User exists
            // Set user session variables
            $this->setUserSession($userData);
        } else {

            // User is new, therefore insert a new user entry
            // Insert user into Database
            $this->_registerFbUser($fbUser);
            $userData = $this->fb_user_model->getFbUser($fbUser);
            // Set user session variables
            $this->setUserSession($userData);
        }
        // Redirect to the user's profile
        echo json_encode(array('redirect' => site_url('profile/' . $userData->profile_path)));
        exit;
    }

    private function _registerFbUser($fbUser) 
    {
        $date = date('Y-m-d H:i:s');
        $registerData = $fbUser;
        $registerData['registered'] = $date;
        $registerData['actual_name'] = $fbUser['fullname'];
        $registerData['profile_path'] = RegisterHelper::crc64($registerData['email'] . time(), '%u');
        $registerData['status'] = 0;

        $this->fb_user_model->addNewFbUser($registerData);
    }
    
    public function setUserSession($userData)
    {
        if ($userData !== false) {
            if ($userData->user_status == 2) {
                echo json_encode(array('redirect' => site_url('login/?login_attempt=2')));
                exit;
            } else {
                $userLoginDate['email'] = $userData->email;
                $userLoginDate['date'] = date('Y-m-d H:i:s');
                $this->fb_user_model->updateFbUserLoginDate($userLoginDate);

                $newUserArray = array(
                    'pid'           => $userData->id,
                    'member'        => $userData->email,
                    'privilege'     => $userData->privileges,
                    'fullname'      => $userData->actual_name,
                    'profile_path'  => $userData->profile_path,
                    'profile_image' => $userData->profile_image
                );

                $this->session->set_userdata($newUserArray);
            }
        }
    }

    public function emailReset()
    {
        $userData = $this->user_model->resetUser($loginCredentials);
    }
}

?>
