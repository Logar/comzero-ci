<?php
/* filename: profile.php */

/* Include the Template class */
include_once('template.php');

/**
 * Prepares user profile pages
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Profile extends MY_Controller
{
    private $_template;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('profile_model', 'profile_model');
        $this->load->model('article_model', 'article_model');

        $this->data['module'] = 'profile';

        $this->data['css'] = array_merge(
            $this->data['css'],
            array(
                'assets/css/profile.css',
                'assets/css/vendor/redactor.css'
            )
        );

        $this->data['scripts'] = array_merge(
            $this->data['scripts'],
            array(
                'assets/scripts/plugins/redactor/redactor.min.js'
            )
        );

        // Call the template class
        $this->_template = Template::getTemplateInstance($this);
    }

    public function displayProfile() {

        $profile = $this->uri->segment(2);

        if(!empty($profile)) {
            $userData = $this->profile_model->getUser($profile);

            if($userData == false) {
                $this->_template->error();
            } else {
                $title = ucwords($userData->actual_name);
                $this->data['page'] = $this->data['page_title'] = $title;
                $this->data['owner'] = $userData->email;
                $this->data['fullname'] = ucwords($userData->actual_name);
                $this->data['subtitle'] = Utilities::setUserTitle($userData->privileges->title);
                $this->data['profilePath'] = $userData->profile_path;
                $this->data['profileImage'] = $userData->profile_image;
                $this->data['newsAlerts'] = false;
                $this->data['profileData'] = $this->profile_model->getUserProfile($userData->id);
                $this->data['userStats'] = $this->profile_model->getUserStatsCount($userData->id);
                $this->data['userArticles'] = $this->article_model->getUserArticles($userData->id);

                $this->_template->generalTemplate(
                    $this->_template->initTemplate("profile/user_profile.phtml", "one_col", $this->data)
                );
            }
        } else {
            $this->_template->error();
        }
    }

    public function editProfile()
    {
        if (isset($_POST['biography'])) {

            $profileData['pid'] = $this->userSession['pid'];
            $profileData['creation_date'] = date('Y-m-d H:i:s', time());
            $profileData['biography'] = $_POST['biography'];

            $this->profile_model->updateProfile($profileData);

            $location = explode('?', $_SERVER['HTTP_REFERER']);
            header("Location: " . $location[0]);
            exit;
        }
    }

    public function editProfileImage()
    { 
        if (isset($_FILES['primary_image'])) {
            $photoData = new stdClass();

            $photoData->primary = $_FILES['primary_image'];
            $photoData->path = 'assets/images/profiles/';

            $resp = $this->_uploadProfileImage($photoData);

            if ($resp['status'] == 1) {
                $articleData['primary_image'] = $resp['primary_image'];
                $articleData['thumb_image'] = $resp['thumb_image'];

                if (isset($resp['headline_thumb'])) {
                    $articleData['headline_thumb'] = $resp['headline_thumb'];
                } else {
                    $articleData['headline_thumb'] = null;
                }
            }
        }
    }

    private function _uploadProfileImage($photoData) {

        $resp['status'] = 0;
        $resp['msg'] = "";

        if (count($photoData->primary) >= 1) {
            if ($photoData->primary["size"] < 16777216) {
                if (($photoData->primary["type"] == "image/gif")
                    || ($photoData->primary["type"] == "image/jpeg")
                    || ($photoData->primary["type"] == "image/pjpeg")
                    || ($photoData->primary["type"] == "image/png")) {

                    $img = getimagesize($photoData->primary["tmp_name"]);
                    $minimum = array('width' => '500', 'height' => '500');
                    $maximum = array('width' => '9000', 'height' => '9000');
                    $width = $img[0];
                    $height = $img[1];

                    if ($width < $minimum['width']) {
                        return $resp;
                    } else if ($height < $minimum['height']) {
                        return $resp;
                    }
                    if ($photoData->primary["error"] > 0) {
                        $resp['status'] = 0;
                        return $resp;
                    }
                    else {
                        $ext = '';

                        $currTime = time();
                        if ($photoData->primary["type"] == "image/gif") {
                            $ext = ".gif";
                        } else if ($photoData->primary["type"] == "image/jpeg") {
                            $ext = ".jpg";
                        } else if ($photoData->primary["type"] == "image/png") {
                            $ext = ".png";
                        } else if ($photoData->primary["type"] == "image/pjpeg") {
                            $ext = ".png";
                        }

                        $new_md5 = md5($currTime);
                        $file_name = $new_md5;
                        $primary_path = $photoData->path . $file_name . $ext;

                        move_uploaded_file($photoData->primary["tmp_name"], $primary_path);
                        $thumb_path = $photoData->path . 'thumbs/' . $file_name . '_t' . $ext;
                        $headlineThumbPath = $photoData->path . 'thumbs/' . $file_name . '_ht' . $ext;

                        require_once('misc/phpthumb/ThumbLib.inc.php');

                        try {
                            $full = PhpThumbFactory::create($primary_path);
                            $full->adaptiveResize(700, 400);
                            $full->cropFromCenter(700, 400)->save($primary_path);

                            $thumb = PhpThumbFactory::create($primary_path);
                            $thumb->adaptiveResize(295, 295);
                            $thumb->cropFromCenter(295, 295)->save($thumb_path);
                            
                            $resp['status'] = 1;
                            $resp['primary_image'] = $primary_path;
                            $resp['thumb_image'] = $thumb_path;

                            return $resp;
                        }
                        catch (Exception $e) {
                            // handle error here however you'd like
                            $resp['status'] = $e->getMessage();
                            return $resp;
                        }
                    }
                }
                else {
                    $resp['status'] = 4;
                    return $resp;
                }
            } else {
                $resp['status'] = 2;
                return $resp;
            }
        } else {
            $resp['status'] = 4;
            return $resp;
        }
    }
}

