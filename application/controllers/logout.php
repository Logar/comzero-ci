<?
/* filename: logout.php */

/**
 * Controller for logging a user out
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Logout extends MY_Controller {

    public function __construct() {

        // Call the base class constructor
        parent::__construct();
    }

    public function index() {

        // Unset all CodeIgniter session variables
        $this->session->sess_destroy();

        /**
         * Delete cookies - the time must be in the past,
         * so just negate what you added when creating the
         * cookie.
         */

        if(isset($_COOKIE['ci_session'])){
            setcookie ("ci_session", "", time()-60*60*24*30, '/', SITE_URL);
        }

        header("Location: " . SITE_URL);
        exit;
    }
}

?>
