<?php
/* filename: article.php */

/* Include the Template class */
include_once('template.php');

/**
 * Article controller
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Article extends MY_Controller
{
    private $_template;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('article_model', 'article_model');
        $this->load->model('general_model', 'general_model');

        // Call the template class
        $this->_template = Template::getTemplateInstance($this);
    }

    public function displayArticle() {

        $articleSlug = $this->uri->segment(2);

        if(!empty($articleSlug)) {
            $article = $this->article_model->getSingleArticleWithSlug($articleSlug);

            if($article == false) {
                $this->_template->error();
            } else {
                $this->data['page'] = $article['title'];
                $this->data['page_title'] = $article['title'];
                $this->data['scripts'] = array('assets/scripts/plugins/jquery.printPage.js');
                $this->data['module'] = 'base';
                $this->data['articles'] = $article;
                $this->data['comments'] = $this->article_model->getArticleComments($article['id']);
                $this->data['videos'] = $article['video_url'];

                $this->getSidebarDependencies();
                
                $this->_template->generalTemplate(
                        $this->_template->initTemplate("article/single.phtml", "two_cols", $this->data, "article")
                );
            }
        } else {
            $this->_template->error();
        }

    }

    function friendlyUrl($str) {

        $friendlyURL = preg_replace('/\s+/', '-', $str);
        $friendlyURL = htmlentities($friendlyURL, ENT_COMPAT, "UTF-8", false);
        $friendlyURL = preg_replace('/&([a-z]{1,2})(?:acute|lig|grave|ring|tilde|uml|cedil|caron);/i','\1',$friendlyURL);
        $friendlyURL = html_entity_decode($friendlyURL,ENT_COMPAT, "UTF-8");
        $friendlyURL = preg_replace('/[^a-z0-9-]+/i', '', $friendlyURL);
        $friendlyURL = preg_replace('/-+/', '-', $friendlyURL);
        $friendlyURL = trim($friendlyURL, '-');
        $friendlyURL = strtolower($friendlyURL);
        return $friendlyURL;

    }

    public function addComment($articleId) {

        if (isset($this->userSession['fullname']) && !empty($this->userSession['fullname'])) {
            $commentData['author'] = $this->userSession['fullname'];
        } else {
            $commentData['author'] = "Guest";
        }

        $commentData['articleId'] = $articleId;
        $commentData['content'] = $_POST['comment'];
        $commentData['currentDate'] = date('Y-m-d H:i:s', time());

        $createdComment = $this->general_model->createComment($commentData);

        if($createdComment) {
            if(isset($_SERVER['HTTP_REFERER'])) {

                header("Location: " . $_SERVER['HTTP_REFERER'] . "#all-comments-container");
                exit;
            } else {
                header("Location: " . SITE_URL);
                exit;
            }
        } else {
            header("Location: ".$_SERVER['HTTP_REFERER']);
            exit;
        }
    }

    // Gets UTC time for the MySQL database
    function setTime() {
        date_default_timezone_set('EST');
        $utc_time = date('Y-m-d H:i:s', time());

        return $utc_time;
    }

    function deleteArticle($id) {

        if(isset($this->isauth2) || isset($this->isauth0)) {

            $article = $this->article_model->deleteArticle($id);

            if(!$article) {
                $data['page'] = 'Not Found';
                Article::page("page_missing", $data);
            } else {
                header("Location: ".SITE_URL);
                exit;
            }
        } else {
            $data['page'] = 'Not Found';
            Article::page("page_missing", $data);
        }
    }

    function deleteComment($id) {

        if(isset($this->isauth2) || isset($this->isauth0)) {

            $comment = $this->general_model->deleteComment($id);

            if(!$comment) {
                $data['page'] = 'Not Found';
                Article::page("page_missing", $data);
            } else {
                header("Location: ".$_SERVER['HTTP_REFERER']);
                exit;
            }
        } else {
            $data['page'] = 'Not Found';
            Article::page("page_missing", $data);
        }
    }

    function printArticle($slug) {

        $data['page_title'] = 'Community Zero';
        $data['article_entry'] = $this->article_model->getSingleArticle($this->user, $slug);

        $data['header'] = $this->load->view('_blocks/header', $data, true);
        $this->load->view('layouts/printer_layout',$data);
    }
}
