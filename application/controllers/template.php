<?php
/* filename: Template.php */

/**
* Class for generating page template
*
* @author     Ross Arena
* @copyright  2012-2013 University of Central Florida
* @license    http://www.php.net/license/3_01.txt  PHP License 3.01
* @version    Release: 1.0
*/
class Template
{
    private static $template = null;
    private $core;

    private function __construct($coreInstance) {

        // Get $this from MOC_Controller class and assign it $this variable
        $this->core = $coreInstance;

        // Load the userdata model
        $this->core->load->model('user_model');
    }

    public static function getTemplateInstance($coreInstance) {
        if(self::$template === null) {
            self::$template = new Template($coreInstance);
        }
        return self::$template;
    }

    /**
     * Generates the view templates for general pages
     *
     * @param string $page
     * @param string $layoutType
     * @access public
     */
    public function generalTemplate($params) {
        //$page, $layoutType, $data
        $params->data[] = $this->sharedTemplate($params);

        $params->data['header'] =  $this->core->load->view('partials/header', $params->data, true);
        $params->data['menu'] =  $this->core->load->view('menu/menu', $params->data, true);
        $params->data['side_menu'] =  $this->core->load->view('menu/vertical_menu.phtml', $params->data, true);

        if($params->layout->type == "two_cols") {
            $params->data['sidebar'] = $this->core->load->view('sidebars/sidebar', $params->data, true);
        }
        $params->data['footer'] =  $this->core->load->view('footers/general_footer.phtml', $params->data, true);

        $this->core->load->view('layouts/universal.phtml', $params->data);
    }

    private function sharedTemplate($params) {
        $params->data['page_title'] = $this->core->data['page_title'];

        if(!isset($params->data['page'])) {
            $params->data['page'] = $this->core->data['page_title'];
        }

        if($params->page !== "none") {
            $params->data['content'] = $this->core->load->view($params->page, $params->data, true);
        }

        $params->data['layout'] = $params->layout;

        return $params;
    }

    public function initTemplate($page, $layout, $data, $layoutSubtype = null) {
        $params = new stdClass;
        $params->page = $page;
        $params->layout = new stdClass;
        $params->layout->type = $layout;
        $params->layout->subtype = $layoutSubtype;
        $params->data = $data;

        $params->data['newsAlerts'] = (isset($params->data['newsAlerts']) ? $params->data['newsAlerts'] : true);

        return $params;
    }

    /**
     * Generates the view templates for admin pages
     *
     * @param string $page
     * @param string $layoutType
     * @access public
     */
    public function adminTemplate($params) {

        $params->data[] = $this->sharedTemplate($params);

        $params->data['header'] =  $this->core->load->view('partials/header', $params->data, true);
        $params->data['menu'] =  $this->core->load->view('menu/admin_menu', $params->data, true);
        $params->data['footer'] =  $this->core->load->view('footers/admin_footer.phtml', $params->data, true);

        $this->core->load->view('layouts/admin.phtml', $params->data);
    }

    /**
     * @param object $postConditions
     * @return string $posts
     */
    public function getPosts($postConditions) {

        if($this->core->userSession['member'] !== false) {
            $posts = $this->core->user_model->getPosts($postConditions);
        } else {
            $posts = $this->core->user_model->getPosts($postConditions);
        }

        return $posts;
    }

    public function error() {

        $this->data['page'] = "Page Not Found";
        $this->data['page_title'] = "Page Not Found";

        $this->generalTemplate(
            $this->initTemplate("page_missing", "two_cols", $this->data)
        );
    }
}

?>