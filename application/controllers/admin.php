<?php
/* filename: admin.php */

/* Include the Template class */
include_once('template.php');

/**
 * This is the main class for administration
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Admin extends MY_Controller
{
    private $_template;

    public function __construct() {

        parent::__construct();

        if (!isset($this->userSession['member']) || $this->userSession['privilege']->level < 2) {
            header('Location: '. SITE_URL);
        }

        $this->load->model('admin_model', 'admin_model');
        $this->load->model('article_model', 'article_model');
        $this->load->model('user_model', 'user_model');
        $this->load->model('general_model', 'general_model');

        $this->data['css'] = array_merge(
            $this->data['css'],
            array(
                'assets/css/admin.css',
                'assets/css/vendor/redactor.css'
            )
        );
        
        $this->data['scripts'] = array_merge(
            $this->data['scripts'],
            array(
                'assets/scripts/admin.js',
                'assets/scripts/plugins/redactor/redactor.min.js'
            )
        );
        $this->data['module'] = 'admin';

        // Call the template class
        $this->_template = Template::getTemplateInstance($this);
    }

    public function index() {

        if (isset($this->userSession['member'])) {
            $this->makePage("admin/index.phtml");
        } else {
            header('Location: '.SITE_URL);
            exit;
        }
    }

    public function searchAlertForm() {

        $this->data['page'] = "Search Alerts";
        $this->data['page_title'] = "Search Alerts";

        $this->makePage("admin/search_alerts.phtml", $this->data);
    }

    public function searchAlertResults() {

        $alertQuery = $_POST['alert'];

        $alertData = $this->admin_model->getMatchedAlert($alertQuery);

        if ($alertData !== false) {
            $request['viewTemplate'] = "searchAlert";
            $request['alerts'] = $alertData;

            $alert = $this->load->view('partials/partials.phtml', $request, true);
            echo json_encode(array('status'=>200, 'msg'=>$alert));
        } else {
            echo json_encode(array(
                'status'=>400,
                'msg'=>"No matches. Please type the full text for the alert
                    or make sure the author is correct."
                )
            );
        }
    }

    public function createAlert() {

        $alertData = $_POST;
        $alertData['date'] = date('Y-m-d H:i:s', time());

        $alert = $this->admin_model->createAlert($alertData);

        if ($alert == true) {
            echo json_encode(array('status'=>200, 'msg'=>"Alert successfully created", "form"=>"remove"));
        } else {
            echo json_encode(array('status'=>400, 'msg'=>"An unexpected error occurred, please retry"));
        }
    }

    public function modifyAlert($alertId) {

        $alertData = $_POST;
        $alertData['id'] = $alertId;
        $alertData['date'] = date('Y-m-d H:i:s', time());

        $alert = $this->admin_model->editAlert($alertData);

        if ($alert == true) {
            echo json_encode(array('status'=>200, 'msg'=>"Alert successfully edited", "form"=>"nothing"));
        } else {
            echo json_encode(array('status'=>400, 'msg'=>"An unexpected error occurred, please retry"));
        }
    }

    public function deleteAlert($alertId)
    {
        $alert = $this->general_model->deleteAlert($alertId);

        header('Location: '.SITE_URL . 'admin');
    }

    public function createAlertForm() {

        $this->data['page'] = "Create News Alert";
        $this->data['page_title'] = "Create News Alert";
        $this->data['slug'] = "createAlert";

        // Setup empty alert array
        $alert = array();
        $alert['source'] = '';
        $alert['message'] = '';
        $this->data['alert'] = $alert;

        $this->makePage("admin/create_alert.phtml", $this->data);
    }

    public function editAlertForm($alertId) {

        $this->data['page'] = "Edit News Alert";
        $this->data['page_title'] = "Edit News Alert";
        $this->data['slug'] = "editAlert";
        $this->data['alert'] = $this->admin_model->getSingleNewsAlert($alertId);

        $this->makePage("admin/create_alert.phtml", $this->data);
    }

    public function searchArticleForm() {

        $this->data['page'] = "Search Articles";
        $this->data['page_title'] = "Search Articles";

        $this->makePage("admin/search_articles.phtml", $this->data);
    }

    public function searchArticleResults() {

        $articleQuery = $_POST['article'];

        $articleData = $this->article_model->getMatchedArticles($articleQuery);

        if ($articleData !== false) {
            $request['viewTemplate'] = "searchArticle";
            $request['articles'] = $articleData;

            $articleList = $this->load->view('partials/partials.phtml', $request, true);
            echo json_encode(array('status'=>200, 'msg'=>$articleList));
        } else {
            echo json_encode(array(
                    'status'=>400,
                    'msg'=>"No matches. Please type the full text for the title
                    or make sure the author's name is correct."
                )
            );
        }
    }

    public function createArticleForm() {

        $this->data['page'] = "Create Article";
        $this->data['page_title'] = "Create Article";
        $this->data['slug'] = "createArticle";

        // Setup empty article array
        $article = array();
        $article['title'] = '';
        $article['is_headline'] = 0;
        $article['category'] = "none";
        $article['caption'] = '';
        $article['prelude'] = '';
        $article['content'] = '';
        $this->data['article'] = $article;

        $this->makePage("admin/create_article.phtml", $this->data);
    }

    public function editArticleForm($articleId)
    {
        $this->data['page_title'] = 'Edit Article';
        $this->data['content'] = '';

        $this->data['slug'] = "editArticle";

        $article = $this->article_model->getSingleArticle($articleId);

        $this->data['article'] = $article;
        $this->data['title'] = htmlentities($article['title'], ENT_QUOTES, "UTF-8");

        $this->makePage("admin/create_article.phtml", $this->data);
    }

    public function searchUsers()
    {
        $userQuery = $_POST['account'];

        $userData = $this->user_model->getMatchedUsers($userQuery);

        if (isset($userData[0]['privileges'])) {
            if ($userData[0]['privileges'] == "admin") {
                $userData = false;
            }
        }

        if ($userData !== false) {
            $request['viewTemplate'] = "searchUsers";
            $request['users'] = $userData;

            $userList = $this->load->view('partials/partials.phtml', $request, true);
            echo json_encode(array('status'=>200, 'msg'=>$userList));
        } else {
            echo json_encode(array(
                    'status'=>400,
                    'msg'=>"No matches. Please make sure you typed the user's
                    fullname or email address."
                )
            );
        }
    }

    public function editUserForm($userId) {

        $this->data['page'] = "Edit User Account";
        $this->data['page_title'] = "Edit User Account";
        $this->data['slug'] = "editUser";
        $this->data['users'] = $this->user_model->getMatchedUsers($userId);

        $this->makePage("admin/edit_user.phtml", $this->data);
    }

    public function deleteArticle($articleId)
    {
        $article = $this->article_model->deleteArticle($articleId);

        header('Location: '.SITE_URL . 'admin/reviewArticles');
    }

    public function modifyArticle($articleId)
    {
        if (isset($_POST['title'])
            && isset($_POST['category'])
            && isset($_POST['content'])
            && isset($_POST['prelude'])) {

            $curr_time = $this->setTime();
            $title = trim($_POST['title']);
            $content = trim($_POST['content']);

            $articleData['article_id'] = $articleId;
            $articleData['slug'] = $this->_friendlyUrl($title);
            $articleData['modified_date'] = date('Y-m-d H:i:s', time());
            $articleData['category'] = $_POST['category'];
            $articleData['title'] = $_POST['title'];
            $articleData['prelude'] = $_POST['prelude'];
            $articleData['content'] = $content;
            $articleData['image_caption'] = (isset($_POST['image_caption']) ? $_POST['image_caption'] : null);
            $articleData['youtube_video'] = (isset($_POST['video_url']) ? $_POST['video_url'] : null);
            $articleData['is_headline'] = 0;

            if (!empty($articleData['youtube_video'])) {
                $video = explode('v=', $articleData['youtube_video']);

                if (!empty($video[1])) {
                    $articleData['youtube_video'] = "http://youtube.com/embed/".$video[1];
                }
            }

            if (isset($_POST['headline'])) {

                if ($_POST['headline'] == 2) {
                    $articleData['is_headline'] = 1;
                }
            }

            if (isset($_POST['top_headline'])) {

                if ($_POST['top_headline'] == 2) {
                    $articleData['is_headline'] = 2;
                }
            }

            if (isset($_FILES['primary_image'])) {
                $photoData = new stdClass;

                if (isset($articleData['is_headline'])) {
                    $photoData->headline = $articleData['is_headline'];
                }

                $photoData->primary = $_FILES['primary_image'];
                $photoData->path = 'assets/images/articles/';

                $resp = $this->uploadPhoto($photoData);

                if ($resp['status'] == 1) {
                    $articleData['primary_image'] = $resp['primary_image'];
                    $articleData['thumb_image'] = $resp['thumb_image'];

                    if (isset($resp['headline_thumb'])) {
                        $articleData['headline_thumb'] = $resp['headline_thumb'];
                    } else {
                        $articleData['headline_thumb'] = null;
                    }
                }
            }

            $result = $this->article_model->editArticle($articleData);

            header("Location: ".SITE_URL . "article/" . $articleData['slug']);
            exit;
        }
        header("Location: ".SITE_URL . "admin/editArticle/$articleId");
        exit;
    }

    public function featureToggles() {

        $this->data['page'] = "Feature Toggles";
        $this->data['page_title'] = "Feature Toggles";

        $this->makePage("admin/feature_toggles.phtml", $this->data);
    }
    
    public function processToggles() {

        if (isset($_POST['responsive'])) {
            if (!isset($_COOKIE['responsive'])) {
                setcookie("responsive", "true", time()+(60*60*24*30), '/');
            }
        } else {
            setcookie("responsive", "true", time() - (60*60*24*30), '/');
        }
        header("Location: ".SITE_URL);
    }

    public function reviewUserArticles()
    {
        $this->data['page'] = "Review Articles";
        $this->data['page_title'] = "Review Articles";

        $this->data['articles'] = $this->article_model->getUserArticles();

        $this->makePage("admin/my_articles.phtml", $this->data);
    }

    public function createArticle()
    {
        if (isset($_POST['title'])
            && isset($_POST['category'])
            && isset($_POST['content'])
            && isset($_POST['prelude'])
            && isset($_POST['image_caption'])) {

        $curr_time = $this->setTime();
        $title = trim($_POST['title']);
        $content = trim($_POST['content']);

        $articleData['author'] = $this->userSession['fullname'];
        $articleData['slug'] = $this->_friendlyUrl($title);
        $articleData['creation_date'] = date('Y-m-d H:i:s', time());
        $articleData['category'] = $_POST['category'];
        $articleData['title'] = $_POST['title'];
        $articleData['prelude'] = $_POST['prelude'];
        $articleData['content'] = $content;
        $articleData['image_caption'] = $_POST['image_caption'];
        $articleData['youtube_video'] = (isset($_POST['video_url']) ? $_POST['video_url'] : null);
        $articleData['is_headline'] = 0;

        if (!empty($articleData['youtube_video'])) {
            $video = explode('v=', $articleData['youtube_video']);

            if (!empty($video[1])) {
                $articleData['youtube_video'] = "http://youtube.com/embed/".$video[1];
            }
        }

        if (isset($_POST['headline'])) {

            if ($_POST['headline'] == 2) {
                $articleData['is_headline'] = 1;
            }
        }

        if (isset($_POST['top_headline'])) {

            if ($_POST['top_headline'] == 2) {
                $articleData['is_headline'] = 2;
            }
        }

        if (isset($_FILES['primary_image'])) {
            $photoData = new stdClass;

            if (isset($articleData['is_headline'])) {
                $photoData->headline = $articleData['is_headline'];
            }

            $photoData->primary = $_FILES['primary_image'];
            $photoData->path = 'assets/images/articles/';

            $resp = $this->uploadPhoto($photoData);

            if ($resp['status'] == 1) {
                $articleData['primary_image'] = $resp['primary_image'];
                $articleData['thumb_image'] = $resp['thumb_image'];

                if (isset($resp['headline_thumb'])) {
                    $articleData['headline_thumb'] = $resp['headline_thumb'];
                } else {
                    $articleData['headline_thumb'] = null;
                }

                $result = $this->article_model->addArticle($articleData);
            } else {
                // Failed to upload image
            }
        }
            header("Location: ".SITE_URL . "article/" . $articleData['slug']);
            exit;
        }
        header("Location: ".SITE_URL . "admin/newArticle/?missing");
        exit;
    }

    private function uploadPhoto($photoData) {

        $resp['status'] = 0;
        $resp['msg'] = "";

        if (count($photoData->primary) >= 1) {

            if ($photoData->primary["size"] < 16777216) {
                if (($photoData->primary["type"] == "image/gif") ||
                    ($photoData->primary["type"] == "image/jpeg") ||
                    ($photoData->primary["type"] == "image/pjpeg") ||
                    ($photoData->primary["type"] == "image/png")) {

                    $img = getimagesize($photoData->primary["tmp_name"]);
                    $minimum = array('width' => '700', 'height' => '400');
                    $maximum = array('width' => '9000', 'height' => '9000');
                    $width = $img[0];
                    $height = $img[1];

                    if ($width < $minimum['width'] ){
                        return $resp;
                    } else if ($height <  $minimum['height']){
                        return $resp;
                    }
                    if ($photoData->primary["error"] > 0) {
                        $resp['status'] = 0;
                        return $resp;
                    }
                    else {
                        $ext = '';

                        $currTime = time();
                        if ($photoData->primary["type"] == "image/gif") {
                            $ext = ".gif";
                        } else if ($photoData->primary["type"] == "image/jpeg") {
                            $ext = ".jpg";
                        } else if ($photoData->primary["type"] == "image/png") {
                            $ext = ".png";
                        } else if ($photoData->primary["type"] == "image/pjpeg") {
                            $ext = ".png";
                        }

                        $new_md5 = md5($currTime);
                        $file_name = $new_md5;
                        $primary_path = $photoData->path . $file_name . $ext;

                        move_uploaded_file($photoData->primary["tmp_name"], $primary_path);
                        $thumb_path = $photoData->path . 'thumbs/' . $file_name . '_t' . $ext;
                        $headlineThumbPath = $photoData->path . 'thumbs/' . $file_name . '_ht' . $ext;

                        require_once('misc/phpthumb/ThumbLib.inc.php');

                        try {
                            $full = PhpThumbFactory::create($primary_path);
                            $full->adaptiveResize(700, 400);
                            $full->cropFromCenter(700, 400)->save($primary_path);

                            $thumb = PhpThumbFactory::create($primary_path);
                            $thumb->adaptiveResize(295, 295);
                            $thumb->cropFromCenter(295, 295)->save($thumb_path);

                            if (isset($photoData->headline)) {
                                $thumb = PhpThumbFactory::create($primary_path);
                                $thumb->adaptiveResize(482, 250);
                                $thumb->cropFromCenter(482, 250)->save($headlineThumbPath);
                                $resp['headline_thumb'] = $headlineThumbPath;
                            }

                            $resp['status'] = 1;
                            $resp['primary_image'] = $primary_path;
                            $resp['thumb_image'] = $thumb_path;

                            return $resp;
                        }
                        catch (Exception $e) {
                            // handle error here however you'd like
                            $resp['status'] = $e->getMessage();
                            return $resp;
                        }
                    }
                }
                else {
                    $resp['status'] = 4;
                    return $resp;
                }
            } else {
                $resp['status'] = 2;
                return $resp;
            }
        } else {
            $resp['status'] = 4;
            return $resp;
        }
    }

    private function _friendlyUrl($str) {

        $friendlyURL = preg_replace('/\s+/', '-', $str);
        $friendlyURL = htmlentities($friendlyURL, ENT_COMPAT, "UTF-8", false);
        $friendlyURL = preg_replace('/&([a-z]{1,2})(?:acute|lig|grave|ring|tilde|uml|cedil|caron);/i','\1',$friendlyURL);
        $friendlyURL = html_entity_decode($friendlyURL,ENT_COMPAT, "UTF-8");
        $friendlyURL = preg_replace('/[^a-z0-9-]+/i', '', $friendlyURL);
        $friendlyURL = preg_replace('/-+/', '-', $friendlyURL);
        $friendlyURL = trim($friendlyURL, '-');
        $friendlyURL = strtolower($friendlyURL);

        return $friendlyURL;
    }

    // Gets UTC time for the MySQL database
    public function setTime() {
        date_default_timezone_set('EST');
        $utc_time = date('Y-m-d H:i:s', time());

        return $utc_time;
    }

    public function searchUsersForm() {

        $this->data['page'] = "Dashboard - Users";
        $this->data['page_title'] = "Dashboard - Users";

        $this->makePage("admin/search_users.phtml", $this->data);
    }

    public function editUserPage($username) {
        $page = "Dashboard - Users";
        $this->data['slug'] = "editUser";

        $this->data['users'] = $this->getUsers();

        $this->data['page'] = $page;
        $this->data['page_title'] = $page;

        $this->makePage("admin/edit_users.phtml", $this->data);
    }

    public function editUserPermissions($pid) {
        $parmas = array();

        $params['privilege'] = $_POST['permissions'];
        $params['pid'] = $pid;

        $userData = $this->user_model->editUserPermission($params);

        if (isset($userData[0]['privileges'])) {
            if ($userData[0]['privileges'] == "admin") {
                $userData = false;
            }
        }

        if ($userData !== false) {

            echo json_encode(array('status'=>200, 'msg'=>"User permissions successfully edited"));
        } else {
            echo json_encode(array(
                    'status'=>400,
                    'msg'=>"An unexpected error occurred, please retry"
                )
            );
        }
    }

    private function getUsers()
    {
        $users = $this->admin_model->getUsers();

        return $users;
    }

    private function makePage($page)
    {
        $this->data['page'] = "Writer Panel";
        $this->data['page_title'] = "Writer Panel";

        $this->_template->adminTemplate(
            $this->_template->initTemplate($page, "one_col", $this->data)
        );
    }

    private function getAllImages()
    {
        $images = array();
        $i = 0;

        foreach(glob(IMAGEPATH.'*') as $filename){
            $images[$i] = $filename;
            $i++;
        }

        return images;
    }
}

?>