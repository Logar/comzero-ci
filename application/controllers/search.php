<?php

/* Include the Template class */
include_once('template.php');

/**
 * Landing controller
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Search extends MY_Controller
{
    private $_template;
        
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
        $this->load->model('general_model');

        // Call the template class
        $this->_template = Template::getTemplateInstance($this);
    }

    /**
     * Starts global search
     * @Method("GET")
     */
    public function startGlobalSearch()
    {
        if (isset($_GET['query']) && !empty($_GET['query'])) {
            $query = $_GET['query'];
            $results = $this->general_model->globalSearch($query);
        } else {
            $results = false;
        }
        
        $this->_displaySearchResults($results);
    }

    private function _displaySearchResults($searchResults)
    {
        $this->data['page'] = $this->data['page_title'] = 'Search Results';
        $this->data['searchResults'] = $searchResults;
        $this->data['newsAlerts'] = false;

        $this->_template->generalTemplate(
            $this->_template->initTemplate(
                "search/global_search.phtml",
                "two_cols",
                $this->data
            )
        );
    }
}
?>
