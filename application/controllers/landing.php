<?php
/* filename: landing.php */

/* Include the Template class */
include_once('template.php');

/**
 * Landing controller
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Landing extends MY_Controller
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('article_model', 'article_model');
        $this->load->model('general_model', 'general_model');
    }

    public function index() {

        // Call the template class
        $template = Template::getTemplateInstance($this);

        $this->data['css'] = array_merge(
            $this->data['css'],
            array(
                '/assets/css/vendor/wowslider.css'
            )
        );
        
        $this->data['scripts'] = array_merge(
            $this->data['scripts'],
            array(
                '/assets/scripts/plugins/slider/slider.js'
                //'/assets/scripts/plugins/mobile-device-detector/mobile-device-detector.js'
            )
        );

        $this->data['inline_scripts'] = $this->data['inline_scripts'] . 
            "$(function() {
                $('#datepicker').datepicker();
            });";

        $this->getSidebarDependencies();

        $this->data['module'] = 'splash';
        $this->data['topCategoryArticles'] = $this->article_model->getTopCategoryArticle(5);
        $this->data['headlineArticles'] = $this->article_model->getHeadlineArticle();
        $this->data['videos'] = $this->general_model->getVideos(5);

        if($this->userSession['member'] == false) {

            // Check if the cookie exists
            if(isset($_COOKIE['comzero_autologin'])) {

                parse_str($_COOKIE['comzero_autologin'], $received);

                $userdata = false; //Landing::get_users_local($received);
                // Make a verification
                if($userdata !== false) {
                    //Login_control Successful. Regenerate session ID to
                    //prevent session fixation attacks
                    session_regenerate_id();

                    $newUserArray = array(
                            'pid'                   => $userdata->id,
                            'member'                => $userdata->user_login,
                            'email'                 => $userdata->user_email,
                            'profile_image_small'   => $userdata->profile_img_src_small,
                            'profile_path'          => $userdata->profile_path
                    );

                    if(!empty($autologin)){
                        setcookie("comzero_autologin", 'usr='.$userdata->user_login.'&hash='.$userdata->user_pass, time()+60*60*24*30);
                    }

                    $this->session->set_userdata($newUserArray);
                }
            }
        }

        $template->generalTemplate(
            $template->initTemplate("splash", "two_cols", $this->data)
        );
    }
}
?>
