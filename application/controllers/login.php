<?php

/* filename: login.php */

/* Include the Template class */
include_once('template.php');
include_once('register.php');

/**
 * Login controller
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Login extends MY_Controller
{
    public function __construct() {

        // Call the Model constructor
        parent::__construct();

        $this->load->model('user_model', 'user_model');
    }

    /**
     * Displays login specific pages
     */
    public function loginPage() {

        // Call the template class
        $template = Template::getTemplateInstance($this);

        if($this->userSession['member'] == false) {

            $this->data['page'] = "Login";
            $this->data['page_title'] = "Login";
            $this->data['newsAlerts'] = false;
            $this->data['scripts'] = array_merge(
                $this->data['scripts'],
                array('assets/scripts/plugins/fb-login/fb-login.js')
            );
            $this->data['module'] = 'login';

            if(isset($_GET['login_attempt'])) {
                $page = "login/login.phtml";
            } else {
                $page = "login/login.phtml";
            }
            $template->generalTemplate(
                $template->initTemplate($page, "one_col", $this->data)
            );
        } else {
            if(isset($_SERVER['HTTP_REFERER'])) {
                header("Location: ".$_SERVER['HTTP_REFERER']);
            } else {
                header("Location: ".SITE_URL);
            }
        }
    }

    public function verify() {

        if(!empty($_POST['email']) && isset($_POST['email'])
            && !empty($_POST['password']) && isset($_POST['password']))
        {
            $email = $_POST['email'];
            $pass = $_POST['password'];

            $loginCredentials['pass'] = Register::generatePassword($pass);
            $loginCredentials['email'] = $email;

            $userData = $this->user_model->getUser($loginCredentials);

            if ($userData !== false) {
                if ($userData->status == 2) {
                    header('Location: /login/?login_attempt=2');
                } else {
                    $userLoginDate['email'] = $email;
                    $userLoginDate['date'] = date('Y-m-d H:i:s');
                    $this->user_model->updateUserLoginDate($userLoginDate);

                    $newUserArray = array(
                        'pid'           => $userData->id,
                        'member'        => $userData->email,
                        'privilege'     => $userData->privileges,
                        'fullname'      => $userData->actual_name,
                        'profile_path'  => $userData->profile_path,
                        'profile_image' => $userData->profile_image
                    );

                    $this->session->set_userdata($newUserArray);

                    header('Location: '.SITE_URL);
                }
            }
            else {
                header('Location: /login/?login_attempt=1');
            }
        }
        else {
            header('Location: /login/?login_attempt=1');
        }
    }

    public function emailReset()
    {
        $userData = $this->user_model->resetUser($loginCredentials);
    }
}

?>
