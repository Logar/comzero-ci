<?php

/**
 * General Services Controller
 *
 * @author     Ross Arena
 * @copyright  2013-2020 Community Zero
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0
 */
class Services extends MY_Controller
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('general_model');
    }

    /**
     * Edits an existing Article entity.
     */
    public function getBreakingNews()
    {
        $news = $this->general_model->getBreakingNews();

        if($news !== false) {
            $responseCode = 200;
        } else {
            $responseCode = 400;
        }

        echo json_encode(array("responseCode"=>$responseCode, "breakingNewsMessages"=>$news));
        exit;
    }

    /**
     * Add email address to mailing list
     * @Method("POST")
     */
    public function addEmailAddress()
    {
        $modelResponse = $this->general_model->addEmailAddress($_POST['mailing_list_email']);

        if($modelResponse !== false) {
            $responseCode = 200;
        } else {
            $responseCode = 400;
        }

        echo json_encode(array("responseCode"=>$responseCode));
        exit;
    }

    public function translations()
    {
        $script = <<<html
            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({
                    pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, 
                    gaTrack: true, gaId: 'UA-41763299-1'}, 'google_translate_element');
                }
            </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
html;
        echo json_encode(array("responseCode"=>200, "message"=>$script));
        exit;
    }
}
?>
