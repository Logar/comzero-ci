<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|   example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|   http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|   $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|   $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
    $route['default_controller'] = "landing";
    $route['404_override'] = "errors";

    // routes for categories
    $route['^(current|history|culture|art|health|opinion|events)$'] = "/page/determineCategory/$0";
    $route['^(current|history|culture|art|health|opinion|events)(:any)'] = "/article/displayArticle/$1";
    // routes for generic pages
    $route['^(about|policy|terms|forgot)$'] = "/page/determinePage/$0";
    // routes for commenting
    $route['add-comment/(:num)'] = "/article/addComment/$1";
    // routes for services
    $route['services/breaking\-news$'] = "/services/getBreakingNews/$0";
    $route['services/mail\-list$'] = "/services/addEmailAddress/$0";
    // routes for reg calls
    $route['^(register)$'] = "/register/registerPage/$0";
    $route['^(login)$'] = "/login/loginPage/$0";
    $route['^(logout)$'] = "/logout/index/$0";
    // routes for articles
    $route['admin/articles$'] = "/admin/viewArticles/$0";
    $route['admin/create-article$'] = "/admin/createArticleForm/$0";
    $route['admin/edit-article/(:num)'] = "/admin/editArticleForm/$1";
    $route['admin/search-articles$'] = "/admin/searchArticleForm/$0";
    $route['admin/review-articles$'] = "/admin/reviewUserArticles/$0";
    // routes for alerts
    $route['admin/create-alert$'] = "/admin/createAlertForm/$0";
    $route['admin/search-alerts$'] = "/admin/searchAlertForm/$0";
    $route['admin/edit-alert/(:num)'] = "/admin/editAlertForm/$1";
    $route['admin/feature-toggles$'] = "/admin/featureToggles/$0";
    // routes for users
    $route['admin/users$'] = "/admin/searchUsersForm/$0";
    $route['admin/edit-user/(:num)'] = "/admin/editUserForm/$1";
    // routes for profile
    $route['profile/editProfile/(:any)$'] = "/profile/editProfile/$1";
    $route['profile/(:any)$'] = "/profile/displayProfile/$1";
    // route for search
    $route['search'] = "/search/startGlobalSearch";


/* End of file routes.php */
/* Location: ./application/config/routes.php */
