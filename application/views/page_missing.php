<br />
<div class="grid_12">
<div style="min-height: 400px;">
<div style="border: 1px solid #ccc; padding: 10px; min-height: 100px;">
<div class="warning-sign"><h2 class="page_title" style="margin-left: 30px;">The page you requested was not found</h2></div><div class="lb"></div><br />
<div class="warning-box">You may have clicked an expired link or mistyped the address. Some web addresses are case sensitive.<br /></div>
<ul>
	<li><a href="<?=SITE_URL ?>">Go back home</a></li>
</ul>
</div>
</div>
</div><div class="clear"></div>
