<?php
    if($session['member'] !== false) {
        if($session['privilege']->level > 1) {
            $accessLink = site_url('admin');
        } else {
            $accessLink = site_url();
        }
        $profileLink = site_url('profile/' . $session['profile_path']);
    } else {
        $accessLink = site_url();
    }
?>

<div id="admin-bar" role="navigation">
    <a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Skip to toolbar</a>
    <div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Top navigation toolbar." tabindex="0">
        <ul id="wp-admin-bar-root-default" class="ab-top-menu">
            <?php 
                if($session['member'] !== false) :
                    if($session['privilege']->level > 1) :
            ?>
            <li id="wp-admin-bar-site-name" class="menupop">
                <a class="ab-item" aria-haspopup="true" href="<?=$accessLink?>">
                Writer Panel
                </a>
            </li>
            <?php
                    endif;
                endif;
            ?>
            <ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
                <li class="float-left">
                    <a href="<?=site_url('about')?>" class="top-header-link">About</a>
                </li>
                <li class="float-left top-header-link">
                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                        <input type="hidden" name="cmd" value="_s-xclick">
                        <input type="hidden" name="hosted_button_id" value="BWFMWG2EWEHLS">
                        <div style="position: relative; width: 80px;">
                            <div style="position: absolute; padding-top: 4px;">
                                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                            </div>
                        </div>
                        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                    </form>
                </li>
                <li class="float-left<?php if($session['member'] === false) { echo ' no-member-search';} ?>">
                    <form method="get" id="searchform" action="<?=site_url('search')?>">
                        <div id="searchwrapper">
                            <div class="input-container">
                                <input type="text" value="" id="search-field" name="query" />
                                <input type="image" src="<?=site_url('/assets/images/search.png') ?>" class="searchbox-submit" alt="search icon" />
                            </div>
                        </div>
                    </form>
                </li>
                <?php if($session['member'] !== false) : ?>
                <li id="wp-admin-bar-my-account" class="menupop with-avatar">
                    <div id="my-account-link" class="ab-item" aria-haspopup="true" title="My Account">
                        <?php
                            $name = explode(' ', $session['fullname']);
                            echo $name[0];
                        ?>
                        <img alt="member-icon" src="<?=media_url('assets/images/member-icon.png')?>" class="avatar avatar-16 photo avatar-mini" height="16" width="16" />
                    </div>
                    <div class="ab-sub-wrapper">
                        <ul id="wp-admin-bar-user-actions" class="ab-submenu">
                            <li id="wp-admin-bar-user-info">
                                <a class="ab-item" tabindex="-1" href="<?=$profileLink?>">
                                    <img alt="member-icon" src="<?=media_url('assets/images/member-icon.png')?>" class="avatar avatar-64 photo" height="64" width="64">
                                    <div class="username"><?=ucwords($session['privilege']->title)?></div>
                                </a>
                            </li>
                            <!--
                            <li id="wp-admin-bar-edit-profile">
                                <a class="ab-item" href="<?=SITE_URL?>wp-admin/profile.php">Edit My Profile</a>
                            </li>
                            -->
                            <li id="wp-admin-bar-logout"><a class="ab-item" href="<?=site_url('logout')?>">Log Out</a></li>
                        </ul>
                    </div>
                </li>
            <? endif; ?>
            </ul>
        </div>
    <a class="screen-reader-shortcut" href="<?=SITE_URL?>logout">Log Out</a>
</div><br />