<h1><?=$page?></h1>
<?php

if(isset($member)) {
    if(in_array($privilege, $auth0) || in_array($privilege, $auth1)) {
        echo '<a href="'.SITE_URL.'article/showCreateForm/'.lcfirst($page).'" class="button create float-right">+ Create</a><div class="clear"></div><br />';
    }
}

if ($articles !== false) {
    $arrSize = sizeof($articles);
    $articleList = '<ul>';
    for($i=0; $i < $arrSize; $i++) {

        $author = ucwords($articles[$i]['author']);
        $profile_path = $articles[$i]['profile_path'];
        $title = $articles[$i]['title'];
        $category = $articles[$i]['category'];
        $article_id = $articles[$i]['article_id'];
        $slug = $articles[$i]['slug'];
        $date = $articles[$i]['creation_date_formatted'];
        $full_date = $articles[$i]['creation_date_full'];
        $prelude = $articles[$i]['prelude'];
        $thumb_image = $articles[$i]['thumb_image'];

        $articleList .= '<li class="article-item">
        <div class="row">
			<div class="col_9 article-thumbnail">            		<a href="' . SITE_URL . $category . '/' . $slug . '"><img src="' . media_url($thumb_image) . '" /></a>        </div>
        <div class="col_15 col_100 break-word">
            <h2><a href="' . SITE_URL . $category . '/' . $slug . '">'.$title.'</a></h2>
            <span class="author-desc">By <a href="' . site_url('profile/' . $profile_path) . '">'.$author.'</a><br />'. $date.'</span><br />
            <p>'.$prelude.' <a href="' . SITE_URL . $category . '/' . $slug . '"><br /><b>Full Article</b></a></p>
        </div>
        </div><div class="clear"></div></li>';
    }
    $articleList .= '</ul>';
    echo $articleList;
} else {
    echo '<br /><div class="warning-box"><b>No articles exist in this category yet. Please come back later.</b></div>';
}
