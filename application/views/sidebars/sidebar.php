<div class="social">
    <img src="<?=media_url('assets/images/social-icons.png')?>" usemap="#ToolbarMap" alt="social icons" />
    <map name="ToolbarMap">
        <area shape="rect" coords="0,14,32,48" href="https://www.facebook.com/pages/Community-Zero/393987974029854" alt="facebook" title="Facebook" target="_blank">
        <area shape="rect" coords="54,14,87,47" href="https://twitter.com/communityzero" alt="twitter" title="Twitter" target="_blank">
        <area shape="rect" coords="109,13,145,48" href="http://www.youtube.com/channel/UCTH6S9Nusi1FD1DO6QYeMTA/" alt="youtube" title="YouTube" target="_blank">
    </map>
</div>
<h2>Event Calendar</h2>
<div id="datepicker"></div><br /><br />
<!--
<h2>Join Our Mailing List</h2>
<div id="mailing-list-container">
    <div class="mailing-add-success success-box hide">
        Welcome to our mailing list!
    </div>
    <form id="mailing-form" action="<?=site_url('services/mail-list')?>" method="post" enctype="multipart/form-data">
        <label>Email Address</label><br />
        <input type="email" name="mailing_list_email" class="width-100" required />
        <input type="submit" value="Subscribe" />
    </form>
</div><br />-->
<? if(isset($opinions)) : ?>
<? if(count($opinions) > 0) : ?>
<h2>Opinions</h2>
<div class="sidebar-block">
<ul class="no-list">
<? foreach($opinions as $item) : ?>
<li></li>
<? endforeach; ?>
</ul>
</div>
<? endif; ?>
<? endif; ?>

<?
if(isset($videos) && !empty($videos)) {

    $video_content = '';
    $video_count = sizeof($videos);

    if($video_count > 0) {

        if(isset($videos[0]['youtube_video'])) {
            echo '<h2>Video Feed</h2>';
            for($i=0; $i < $video_count; $i++) {
                $video = $videos[$i]['youtube_video'];
                $video_content .= '<iframe width="100%" height="245" src="'.$video.'" frameborder="0" allowfullscreen></iframe><br /><br />';
            }
        } else {
            echo '<h2>Article Video</h2>';
            $video_content .= '<iframe width="100%" height="245" src="'.$videos.'" frameborder="0" allowfullscreen></iframe><br /><br />';
        }
        echo $video_content;
    }
}
?>