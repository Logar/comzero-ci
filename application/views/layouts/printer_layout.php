<?=$header?>

<body id="white-bg">
	<div id="wrapper">			
		<div class="container_24" id="printer-content">
			<div class="inner">
				<div class="grid_24">
					<?php

						$author = ucwords($article_entry['author']);
						$entry_id = $article_entry['id'];
						$title = $article_entry['title'];
						$date = $article_entry['article_date_formatted'];
						$entry_content = $article_entry['content'];
						
						echo '<div class="float-left article-item">
						
						<div class="float-left">
						<h2>'.$title.'</h2>
						<span class="author-desc">By <b>'.$author.'</b><br />'. $date.'</span><br /><br />
						<p>'.$entry_content.'</p>
						</div></div><div class="clear"></div>';
					?>
					
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</body>
</html>
