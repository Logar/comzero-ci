<div class="clearfix">
    <div class="row">
        <div class="col_24 col_100">
            <? if(isset($headlineArticles) && !empty($headlineArticles)) : ?>
                <div class="clearfix">
                    <div id="headline-article-slider">
                        <div class="headline-content-container">
                            <ul class="headline-articles">            
                                <? for($i = 0; $i < count($headlineArticles); $i++) : ?>
                                    <? if($i == 4) { break; } ?>
                                    <li class="<? if ($i==0) echo 'show'; else echo 'hide'; ?>">
                                        <h3 id="headarticle_<?=$i?>" class="headline-title">
                                            <a href="<?=site_url($headlineArticles[$i]['category'] . '/' . $headlineArticles[$i]['slug'])?>">
                                                <?=$headlineArticles[$i]['title']?>
                                            </a>
                                        </h3>
                                        <p><?=$headlineArticles[$i]['prelude'] ?></p>
                                    </li>
                                <? endfor; ?>
                            </ul>
                        </div>
                        <div id="wowslider-container1" style="height: 250px;">
                            <!--<a href="#" class="ws_next"></a>
                            <a href="#" class="ws_prev"></a>-->
                            <div class="ws_images">
                                <ul>
                                <? for($i = 0; $i < count($headlineArticles); $i++) : ?>
                                <? if($i == 4) { break; } ?>
                                    <li class="<? if($i == 0) { echo 'show'; } else { echo 'hide'; } ?>">
                                        <a href="<?=site_url($headlineArticles[$i]['category'] . '/' . $headlineArticles[$i]['slug'])?>">
                                            <img src="<?=media_url($headlineArticles[$i]['headline_thumb'])?>" alt="<?=$headlineArticles[$i]['title']?>" class="headline-medium" />
                                        </a>
                                    </li>
                                <? endfor; ?>
                                </ul>
                            </div>
                            <div class="ws_thumbs">
                                <div>
                                <? for($i = 0; $i < count($headlineArticles); $i++) : ?>
                                <? if($i == 4) { break; } ?>
                                    <a href="#" title="<?=$headlineArticles[$i]['title']?>"><img src="<?=media_url($headlineArticles[$i]['thumb_image'])?>"
                                    alt="<?=$headlineArticles[$i]['title'] ?>" class="headline-thumb" /></a>
                                <? endfor; ?>
                                </div>
                            </div>
                            <div class="ws_shadow"></div>
                        </div>
                    </div>
                </div>
            <? endif; ?>
        </div>
</div><br />
<? if(!empty($topCategoryArticles)) : ?>
<br /><h2>Featured Articles</h2>
<ul class="feature-list">
    <? for($i = 0; $i < count($topCategoryArticles); $i++) : ?>
    <li class="category-preview">
        <div class="clearfix">
            <div class="row">
                <div class="col_7 category-preview-image">
                    <a href="<?=site_url($topCategoryArticles[$i]['category'] . '/' . $topCategoryArticles[$i]['slug'])?>"><img src="<?=media_url($topCategoryArticles[$i]['thumb_image'])?>" class="splash-category-thumb" /></a>
                </div>
                <div class="col_17 col_100">
                    <a href="<?=site_url($topCategoryArticles[$i]['category'])?>" class="category-title"><h4><b><?=ucwords($topCategoryArticles[$i]['category'])?></b></h4></a><br />
                    <h2><a href="<?=site_url($topCategoryArticles[$i]['category'] . '/' . $topCategoryArticles[$i]['slug'])?>"><?=$topCategoryArticles[$i]['title']?></a></h2>
                    <span class="author-desc">By <a href="<?=site_url('profile/' . $topCategoryArticles[$i]['profile_path'])?>"><?=$topCategoryArticles[$i]['author']?></a></span><br />
                    <p><?=$topCategoryArticles[$i]['prelude']?></p>
                    <a href="<?=site_url($topCategoryArticles[$i]['category'] . '/' . $topCategoryArticles[$i]['slug'])?>"><b>Full Article</b></a>
                </div>
            </div><br />
        </div>
    </li>
    <? endfor; ?>
</ul>
<? endif; ?>
</div>