<nav id="menuv-container">
    <label for="articles-subnav" class="top-navlink">Articles<span class="arrow-down admin-navarrow"></span></label>
    <input type="checkbox" id="articles-subnav" class="toggle-subnav" />
    <div class="subnav">
        <a href="<?=site_url('admin/review-articles')?>">My Articles</a>
        <? if($session['privilege']->level > 4) : ?>
        <a href="<?=site_url('admin/search-articles')?>">Search Articles</a>
        <? endif; ?>
        <a href="<?=site_url('admin/create-article')?>">New Article</a>
    </div>
    <? if($session['privilege']->level > 2) : ?>
    <label for="alert-subnav" class="top-navlink">News Alert<span class="arrow-down admin-navarrow"></span></label>
    <input type="checkbox" id="alert-subnav" class="toggle-subnav" />
    <div class="subnav">
        <a href="<?=site_url('admin/create-alert')?>">New Alert</a>
    <? endif; ?>
        <? if($session['privilege']->level > 4) : ?>
        <a href="<?=site_url('admin/search-alerts')?>">Search Alerts</a>
        <? endif; ?>
    </div>
    <? if($session['privilege']->level > 5) : ?>
    <a href="<?=site_url('admin/users')?>">Users</a>
    <? endif; ?>
    <? if($session['privilege']->level > 3) : ?>
    <a href="<?=site_url('admin/feature-toggles')?>">Feature Toggles</a>
    <? endif; ?>
    <div class="thick-border"></div>
    <a href="<?=site_url('logout') ?>" class="top_parent">Logout</a>
</nav>