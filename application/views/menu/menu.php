<nav id="menuh-container">
    <div class="block-wrapper">
        <div id="menuh">
            <ul>
                <li><a href="<?=site_url() ?>" class="top_parent<?
                if ($page == "home") : ?> current-nav-link<? endif; ?>">News</a></li>
            </ul>
            <ul>
                <li><a href="<?=site_url('events') ?>" class="top_parent<?
                if ($page == "events") : ?> current-nav-link<? endif; ?>">Events</a></li>
            </ul>
            <ul>
                <li><a href="<?=site_url('current') ?>" class="top_parent<?
                if ($page == "current") : ?> current-nav-link<? endif; ?>">Current</a></li>
            </ul>
            <ul>
                <li><a href="<?=site_url('art') ?>" class="top_parent<?
                if ($page == "art") : ?> current-nav-link<? endif; ?>">Art</a></li>
            </ul>
            <ul>
                <li><a href="<?=site_url('history') ?>" class="top_parent<?
                if ($page == "history") : ?> current-nav-link<? endif; ?>">History</a></li>
            </ul>
            <ul>
                <li><a href="<?=site_url('culture') ?>" class="top_parent<?
                if ($page == "culture") : ?> current-nav-link<? endif; ?>">Culture</a></li>
            </ul>
            <ul>
                <li><a href="<?=site_url('health') ?>" class="top_parent<?
                if ($page == "health") : ?> current-nav-link<? endif; ?>">Health</a></li>
            </ul>
            <ul>
                <li><a href="<?=site_url('opinion') ?>" class="top_parent<?
                if ($page == "opinion") : ?> current-nav-link<? endif; ?>">Opinion</a></li>
            </ul>
        </div>
    </div>
</nav>