<!DOCTYPE html>
<html>
<head>
    <title><?=$page_title?></title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="description" content="Community Zero is an online community and independent news outlet that features reporting, commentary, and analysis on politics, culture, art, history, and more." />
    <meta name="keywords" content="community, the community, online community, news agency, news website, latest news, current news, current news today, local news, community zero" />
    <meta name="author" content="Ross Arena" />
    <meta name="robots" content="index, follow" />
    <meta name="googlebot" content="index, follow" />

    <link rel="shortcut icon" href="<?=site_url('favicon.ico')?>" />
    <?php 
        $styles = array();
        array_push($styles, media_url('assets/css/main.css'));

        if(isset($_COOKIE['responsive'])) {
            array_push($styles, media_url('assets/css/responsive.css'));
        }

        if(isset($css)) {
            $resource = '';
            foreach($css as $path) {
                array_push($styles, media_url($path));
            }
        }

        if(!isset($module)) {
            $module = 'base';
        }

        $file = 'assets/css/modules/' . $module . '/' . $module . '.css';
    ?>
    <link rel="stylesheet" type="text/css" href="<?=media_url($this->minifier->merge($file, 'assets/css/modules/'.$module, $styles));?>" media="screen" />
</head>