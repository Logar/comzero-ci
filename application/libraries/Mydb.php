<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// We need some constants to clear out some things also code wise
define('FETCH_ARRAY',1);
define('FETCH_ASSOC',2);
define('FETCH_OBJECT',3);
define('MYSQL_ERROR_PARAMETER',1);
define('MYSQL_ERROR_CONNECT',2);
define('MYSQL_ERROR_SELECT',3);
define('MYSQL_ERROR_SQL',4);

class Mydb
{
    private $CI, $mysqli, $result;
    
    /**
      * The constructor
    */
    public function __construct() {
        $this->CI = & get_instance();
        $this->mysqli = $this->CI->db->conn_id;
        //var_dump($this->mysqli);
    }
    
    // run query
    public function Query($query) {
	
        $this->result = @$this->mysqli->query($query);
        while ($this->mysqli->next_result()) {
          //free each result.
          $res = @$this->mysqli->use_result();
          if ($res instanceof mysqli_result) { $res->free(); }  
        }
        return new Result($this->mysqli,$this->result);
    }
}

class Result {

    private $mysqli, $result;

    public function __construct(&$mysqli, $result) {
        $this->mysqli = &$mysqli;
        $this->result = $result;
    }

    

    public function fetch($fetchMode=FETCH_OBJECT) {
        if ($fetchMode == FETCH_ARRAY) { $row = @$this->result->fetch_array(MYSQLI_BOTH); }
        if ($fetchMode == FETCH_ASSOC) { $row = @$this->result->fetch_assoc(); }
        if ($fetchMode == FETCH_OBJECT) { $row = @$this->result->fetch_object(); }

        if ($row) { return $row;} 

        else if ($this->numRows() > 0) {
            @$this->result->data_seek(0); 
            return array(); //false;
        } 

        else { return array();}//false; 
    }

    
    public function numRows() { return @$this->result->num_rows; }

    public function affectedRows() { return @$this->result->affected_rows; }

    public function free() { $this->result->free(); }

    public function MySQLError() { return @$this->mysqli->error; }

}

?>