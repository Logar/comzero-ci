<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once("config.php");
include_once("inc/twitteroauth.php");


class TwitterLogin
{
    private $_CI;
    
    /**
     * The constructor
     */
    public function __construct() {
        $this->_CI = & get_instance();
    }
    
    public function processLogin() {
        if (isset($_REQUEST['oauth_token']) && $_SESSION['token'] !== $_REQUEST['oauth_token']) {

            // If token has expired, destroy the session variables and redirect back to the referer
            session_destroy();
            header('Location: ' . $_SERVER['HTTP_REFERER']);
            
        } elseif (isset($_REQUEST['oauth_token']) && $_SESSION['token'] == $_REQUEST['oauth_token']) {

            // Authentication valid, request access token
            // Successful response returns oauth_token, oauth_token_secret, user_id, and screen_name
            $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['token'], $_SESSION['token_secret']);
            $access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
            
            if ($connection->http_code == '200') {
                
                // Redirect user to twitter
                $_SESSION['status'] = 'verified';
                $_SESSION['request_vars'] = $access_token;
                
                // Unset no longer needed request tokens
                unset($_SESSION['token']);
                unset($_SESSION['token_secret']);
                header('Location: ' . SITE_URL);
            } else {
                die("An error was encountered, please try again later!");
            }
                
        } else {
            if (isset($_GET["denied"])) {
                header('Location: ' . $_SERVER['HTTP_REFERER']);
                die();
            }

            // Fresh authentication
            $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
            $request_token = $connection->getRequestToken(OAUTH_CALLBACK);
            
            //received token info from twitter
            $_SESSION['token'] = $request_token['oauth_token'];
            $_SESSION['token_secret'] = $request_token['oauth_token_secret'];
            
            // Any value other than 200 is failure, so continue only if http code is 200
            if ($connection->http_code=='200') {
                // Redirect user to twitter
                $twitterUrl = $connection->getAuthorizeURL($request_token['oauth_token']);
                header('Location: ' . $twitterUrl); 
            } else {
                die("An error was encountered while trying to connect to twitter! Please try again later!");
            }
        }
    }
}