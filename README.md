# [Community Zero](http://community-zero.com)

Community Zero is an online community and independent news outlet that features reporting, commentary, and analysis on politics, culture, art, history, and more.

* Source: [https://bitbucket.org/Logar/comzero-ci](https://bitbucket.org/Logar/comzero-ci)
* Homepage: [http://community-zero.com](http://community-zero.com)


## Quick start

Choose one of the following options:

1. Download the latest stable release from
   [https://bitbucket.org/Logar/comzero-ci](https://bitbucket.org/Logar/comzero-ci).
2. Clone the git repo — `git clone
   https://bitbucket.org/Logar/comzero-ci` - and checkout the latest tagged
   release.


## Features

* Custom administrative backend panel
* Easy-to-use, user-friendly HTML5 editor for editing content
* Responsive layout for mobile devices
* Cross-browser compatible (Chrome, Opera, Safari, Firefox 3.6+, IE10+).
* Designed with progressive enhancement in mind.
* Includes [Normalize.css](http://necolas.github.com/normalize.css/) for CSS
  normalizations and common bug fixes.
* The latest [jQuery](http://jquery.com/) via CDN, with a local fallback.
* The latest [Modernizr](http://modernizr.com/) build for feature detection.
* Apache server caching, compression, and other configuration defaults for
  Grade-B performance.
* Extensive inline and accompanying documentation.


## Documentation

Take a look at the [documentation table of contents](doc/TOC.md). This
documentation is bundled with the project, which makes it readily available for
offline reading and provides a useful starting point for any documentation you
want to write about your project.


## Contributing

Please contact communityzeropress@gmail.com if you are interested in becoming a developer for this project.