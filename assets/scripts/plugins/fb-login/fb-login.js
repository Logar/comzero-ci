(function($) {
    
    (function() {
        var e = document.createElement('script');
        e.async = true;
        e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
        document.getElementById('fb-root').appendChild(e);
        initFbSession();
    }());

    function initFbSession() {
        window.fbAsyncInit = function() {
            FB.init({
               appId: '491493244268935',
               cookie: true,
               xfbml: true,
               oauth: true
            }); 
        };
    }
    
    $("#fb-login").click(function(e) {
        e.preventDefault();
        // verify facebook login
        verifyFbLogin();
        return false;
    });

    function verifyFbLogin() {
        setAnimation();
        FB.login(function(response) {
            if (response.authResponse) {
                FB.api('/me?scope=name,email', function(resp) {
                    var userData = "fullname=" + encodeURIComponent(resp.name) + "&username=" + encodeURIComponent(resp.username);
                    // JSON.stringify(resp);
                    ajaxFbResponse(userData);
                });
            } else {
                console.log('User cancelled login or did not fully authorize.');
            }
        }, {scope:'email,status_update,publish_stream'});
    }

    function ajaxFbResponse(dataString) {
        $.ajax
        ({
            type: "POST",
            url: site_url + 'fbLogin/getFbUser',
            async: true,
            data: dataString,
            dataType: "json",
            cache: false,
            success: function(resp) {
                window.location = resp.redirect;
                resetAnimation();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                resetAnimation();
                window.location = site_url;
            }
        });
    }

    // Display loading image
    function setAnimation() {
         // Show ajax loader while processing the request
         $('#login-loader').prepend('<img src="' + site_url + 'assets/images/loading.gif" id="login-loader" /> Please Wait Connecting...');
    }

    // Remove loading image
    function resetAnimation() {
        $('#login-loader').html();
    }
    
})(jQuery);