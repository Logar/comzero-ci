(function($) {

    this.index = -1;
    this.imageLength = 3;
    this.timeout = 4000;
    
    $('#headline-article-slider').mouseenter(sliderIn).mouseleave(sliderOut);

    $('.ws_prev').click(function(e) {
        e.preventDefault();
        this.index = this.index - 1;
    });

    $('.ws_next').click(function(e) {
        e.preventDefault();
        this.index = this.index + 1;
    });

    function sliderIn() {
        $('.ws_next, .ws_prev').show();
    }

    function sliderOut() {
        $('.ws_next, .ws_prev').hide();
    }

    $('.ws_thumbs a').click(function(e) {
        e.preventDefault();
    });

    (function startSlider() {
        startTimeout();
    })();

    function getNextImage(defer) {

        if (this.index == -1) {
            resetThumbs();
        } else {
            slideThumbs();
        }
        
        $('.ws_images ul > li').eq(this.index).hide();
        this.index = this.index + 1;
        $('.ws_images ul > li').eq(this.index).show();

        getNextArticle();

        defer.resolve();
    }

    function getNextArticle() {
        $('.headline-articles li').hide();
        $('.headline-articles li').eq(this.index).show();
    }

    function slideThumbs() {
        $('#wowslider-container1 .ws_thumbs div').css("left", "-=75");
    }

    function resetThumbs() {
        $('#wowslider-container1 .ws_thumbs div').css("left", "0");
    }

    function startTimeout() {

        var defer = $.Deferred();

        setTimeout(function() {

            if (this.index == this.imageLength) {
                resetIndex();
            }
            getNextImage(defer);
        }, this.timeout);

        defer.done(function() {
            startTimeout();
        });
    }

    function resetIndex() {
        this.index = -1;
    }
 
})(jQuery);