$(function() {
    $('.delete').click(function(e) {
        e.preventDefault();
        var href = this.href;

        var answer = confirm("Are sure you want to delete this?");
        if (answer) {
            window.location.href = href;
        } else {
            return false;
        }
    });

    $('.change-image').click(function(e) {
        e.preventDefault();
        $(this).hide();
        $('.uploaded-image').attr("required","required");
        $('.uploaded-image').show();
        $('.cancel-image-change').show();
    });

    $('.cancel-image-change').click(function(e) {
        e.preventDefault();
        $(this).hide();
        $('.uploaded-image').removeAttr('required');
        $('.uploaded-image').hide();
        $('.change-image').show();
    });

    $('.ajax-form-single').submit(function(event) {
        var $this = $(this);
        // stop form from submitting normally
        event.preventDefault();

        // On submit disable its submit button
        $('input[type=submit]', this).attr('disabled', 'disabled');

        var form = $(this), currUrl = form.attr('action');

        var dataString = form.serialize();
        $.ajax
        ({
            type: "POST",
            url: currUrl,
            data: dataString,
            dataType: "json",
            cache: false,
            success: function(resp) {
                if(resp.form == "remove") {
                    $this.remove();
                }

                if(resp.status == 200) {
                    $(".error-box").hide();
                    $('.success-box').show().empty().append(resp.msg);
                } else {
                    $(".success-box").hide()
                    $(".error-box").show().empty().append(resp.msg);
                }

                // When results returned, enable submit button
                $('input[type=submit]', $this).removeAttr('disabled');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });

        return false;
    });

    $('.ajax-form-multi').submit(function(event) {
        var $this = $(this);
        // stop form from submitting normally
        event.preventDefault();
        var form = $(this), currUrl = form.attr('action');

        var dataString = form.serialize();
        $.ajax
        ({
            type: "POST",
            url: currUrl,
            data: dataString,
            dataType: "json",
            cache: false,
            success: function(resp) {
                if(resp.status == 200) {
                    $(".error-box").hide();
                    $('.results-box').show().empty().append(resp.msg);
                } else {
                    $('.results-box').show().empty().append(resp.msg);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });

        return false;
    });

    $('.uploaded-image').change(function(e) {
        e.preventDefault();
        if(this.disabled) return alert('File upload not supported!');
        var F = this.files;
        if(F && F[0]) for(var i=0; i<F.length; i++) readImage(F[i], $(this));
    });

    function readImage(file, $this) {

        var reader = new FileReader();
        var image  = new Image();

        reader.readAsDataURL(file);
        reader.onload = function(_file) {
            image.src = _file.target.result;              // url.createObjectURL(file);
            image.onload = function() {
                var w = this.width,
                    h = this.height,
                    t = file.type,                           // ext only: // file.type.split('/')[1],
                    n = file.name,
                    s = ~~(file.size/1024) +'KB';

                if(w < 700 || h < 400) {
                    alert("Minimum dimensions are 700px by 400px. Current image is "+w+ "px by "+h+"px");
                    resetFormElement($this);
                    $('#uploadPreview').empty();
                } else {
                    $('#uploadPreview').empty().append('<b>Preview Not Actual Image Size</b><br><img src="'+ this.src +'" class="splash-category-thumb"><br>'+w+'x'+h+' '+s+' '+t+' '+n+'<br><br>');
                }
            };
            image.onerror= function() {
                alert('Invalid file type: '+ file.type);
            };
        };
    }

    function resetFormElement(ele) {
        ele.wrap('<form>').closest('form').get(0).reset();
        ele.unwrap();
    }
});