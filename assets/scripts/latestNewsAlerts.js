(function($) {
    
    // Plugin Name
    var PLUGIN_NAME = 'latestNewsAlerts';

    var iterateNewsItems = function (target, message, index, interval, dfd) {
        if (index < message.length) {
            $(target).append(message[index++]);
            setTimeout(iterateNewsItems.bind(this, target, message, index, interval, dfd), interval);
        } else {
            dfd.resolve();
        }
    };
 
    var methods = {

        init: function(options) {
            $this = $(this);
            this.getMe = 0;
            // Repeat over each element in selector
            return this.each(function() {
                var $this = $(this);

                // Attempt to grab saved locals, if they don't exist we'll get "undefined".
                var locals = $this.data(PLUGIN_NAME);
 
                // If we could't grab locals, create them from defaults and passed options
                if(typeof(locals) == 'undefined') {
 
                    var defaults = {
                        headlineMessages: new Array(),
                        gotBreakingNewsOnce: false
                    }
 
                    locals = $.extend({}, defaults, options);
 
                    // Save our newly created locals
                    $this.data(PLUGIN_NAME, locals);
                } else {
                    // We got locals, merge our passed options in with them (optional)
                    locals = $.extend({}, locals, options);
                }

                $('#news-message').empty();
                var path = $('#breaking-news-path').attr('href');

                if(locals.gotBreakingNewsOnce == false) {
                    $.ajax
                    ({
                        type: "POST",
                        url: path,
                        async: false,
                        dataType: "json",
                        contentType: 'application/json',
                        cache: true,
                        success: function(resp) {
                            if(resp.responseCode == 200) {
                                locals.headlineMessages = resp.breakingNewsMessages;
                                locals.gotBreakingNewsOnce = true;

                            } else if(resp.responseCode == 400) {
                                console.log('Failed to get headline messages');
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                        }
                    });
                }
            });
        },
        showNewsFeed : function (selector, index) {
            
            var that = this;
            var locals = that.data(PLUGIN_NAME);
            
            if (typeof selector == 'undefined') {
                selector = '#news-message';
            }
            if (typeof index == 'undefined') {
                index = 0;
            }
            
            if (typeof locals.headlineMessages[index] !== 'undefined' && locals.headlineMessages[index] !== null) {
                // Create a deferred object
                var defer = $.Deferred();

                iterateNewsItems(selector, locals.headlineMessages[index].message, 0, 50, defer);

                defer.done(function() {
                    setTimeout(function() {
                        $('#news-message').empty();
                        index = index + 1;
                        methods.showNewsFeed.call(that, '#news-message', index);
                    }, 6000);
                });

            } else {
                return methods.showNewsFeed.call($this, '#news-message', 0);
            }
        },
        destroy: function(options) {
            // Repeat over each element in selector
            return $(this).each(function() {
                var $this = $(this);
 
                // Remove locals data when deallocating our plugin
                $this.removeData(PLUGIN_NAME);
            });
        }
    };
 
    $.fn.latestNewsAlerts = function() {
        var method = arguments[0];
 
        if(methods[method]) {
            method = methods[method];
            arguments = Array.prototype.slice.call(arguments, 1);
        } else if( typeof(method) === 'object' || typeof method === 'undefined') {
            method = methods.init;
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.pluginName' );
            return this;
        }
        return method.apply(this, arguments);
    }
 
})(jQuery);