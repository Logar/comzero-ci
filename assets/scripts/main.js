(function($) {

    //$(this).mobilecheck();

    $('.single_comment_entity').hover(
        function() {
            var child = $(this).children('.comment-delete:first');
            child.show();
        },
        function() {
            var child = $(this).children('.comment-delete:first');
            child.hide();
        }
    );
    
    // Adjust height of overlay to fill screen when page loads
    $("#login-panel").css("height", $(window).height());

    // When the link that triggers the message is clicked fade in overlay/msgbox
    $("#login-link").click(function(e){
        e.preventDefault();
        $("#login-panel").fadeIn();
        return false;
    });

    // When the message box is closed, fade out
    $(".close").click(function(){
        $(this).closest(".dim").fadeOut();
        return false;
    });

    $("#wp-admin-bar-my-account").click(myAccountLink);

    function myAccountLink() {
        $(this).children('.ab-sub-wrapper').toggle();
    }

    $("#register-form").submit(function(event) {

        var $this = $(this);
        // stop form from submitting normally
        event.preventDefault();
        var form = $(this), currUrl = form.attr('action');

        var dataString = $(this).serialize();
        $.ajax
        ({
            type: "POST",
            url: currUrl,
            data: dataString,
            dataType: "json",
            cache: false,
            success: function(resp) {
                if(resp.status == 1) {
                    $('#register-form').remove();
                    $this.prepend(resp.msg);
                    setTimeout(window.location.href = site_url, 1000);
                } else {
                    $("#error-msg").empty().append(resp.msg);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });

        return false;
    });

    $("#mailing-form").submit(function(event) {

        var $this = $(this);
        // stop form from submitting normally
        event.preventDefault();
        var form = $(this), currUrl = form.attr('action');

        var dataString = $(this).serialize();
        $.ajax
        ({
            type: "POST",
            url: currUrl,
            data: dataString,
            dataType: "json",
            cache: false,
            success: function(resp) {
                if(resp.responseCode == 200) {
                    $this.children().hide();
                    $('.mailing-add-success').show();
                } else {
                    $("#error-msg").empty().append(resp.msg);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });

        return false;
    });

    $("#start-translations").click(function(event) {

        var $this = $(this);
        event.preventDefault();

        $.ajax
        ({
            type: "POST",
            url: site_url + 'services/translations',
            dataType: "json",
            cache: false,
            success: function(resp) {
                if(resp.responseCode == 200) {
                    $('#start-translations').hide();
                    console.log(resp.message);
                    eval("("+resp.message+")");
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });

        return false;
    });

    $('.go-top').click(function() {
        $('html, body').animate({scrollTop:0}, 0);
        return false;
    });

    if($('.breaking-news').length > 0) {
        $('#news-message').latestNewsAlerts();
        $('#news-message').latestNewsAlerts('showNewsFeed');
    }
})(jQuery);