function ConvertDates() {

    this.currentTimeZone = 0;

    this.getTimezone = function() {
        return this.currentTimeZone;
    };

    this.setTimezone = function() {
        this.currentTimeZone = new Date().getTimezoneOffset();
    };

    this.findDates = function() {
        var dates = $j('.datetime');
    };
}

ConvertDates.prototype.setDates = function(dates) {
    $j.each(dates, function(index, value) {
        $j(this).text(value);
    });
};