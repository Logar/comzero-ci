// Require dependencies
var util = require('util'),
    less = require('less'),
    fs = require('fs');

// Config file
var config = 'bundles.json';

fs.readFile(config, 'utf8', function(err, data) {
    if (err) {
        throw err;
    }
    
    data = JSON.parse(data);
    //console.log(data);
    iterateNodes(data);
    //console.log(data.bundles.bundle['type']);
});

function iterateNodes(data) {
    for (var i = 0, l = data.nodes.length; i < l; i++) {
        var node = data.nodes[i];
        
        console.log(node.name);
        
        if (node.nodes) {
            arguments.callee(node);
        }
    }
}