<?php

include_once('../../vendor/oyejorge/less.php/lessc.inc.php');
define('REPORT_DEST_DIR', '/target/minify-reports/');
define('MINIFY_CONFIG_FILE', 'minify-config.xml');
define('REPORT_TEMPLATE_FILE', 'minify-report-template.html');
define('TARGET_DIR', '/target');
define('DOCUMENT_ROOT', '../..');

function minifyReport() 
{
    echo "Starting minify reporter...";
    $stats = array();
    $sizes = array();
    $tplPath = REPORT_TEMPLATE_FILE;
    $reportDir = REPORT_DEST_DIR;
    $reportOut = $reportDir . 'minify-report.html';
    $layout = file_get_contents($tplPath, "r");
    
    if (!function_exists('simplexml_load_file')) {
        echo "ERROR: The SimpleXML Library is required to run the Minify Reporter";
        die(1);
    }
    $data = simplexml_load_file(MINIFY_CONFIG_FILE);
    $bundles = $data->bundle;
    if (!$bundles) {
        echo "ERROR: No Minify Bundles were detected in the config file:" . MINIFY_CONFIG_FILE;
        die(1);
    }
    
    foreach ($bundles as $bundle) {
        $stat = new stdClass;
        $stat->bundleType = (string) $bundle->type;
        $stat->files = $bundle->files;
        $stat->targetDir = (string) $bundle->webappTargetDir;
        $stat->fileOut = (string) $bundle->outputFile;
        $stat->fileOutPath = TARGET_DIR . $stat->targetDir . '/' . $stat->fileOut;
        // In Kb
        $stat->totalSize = 50; //round(filesize($stat->fileOutPath) / 1024, 2);
        if (!isset($stats[$stat->bundleType])) {
            $stats[$stat->bundleType] = array();           
        }
        $stats[$stat->bundleType][] = $stat;

        if (!isset($sizes[$stat->bundleType])) {
            $sizes[$stat->bundleType] = $stat->totalSize;           
        } else {
            $sizes[$stat->bundleType] += $stat->totalSize;
        }

        if ($stat->bundleType == 'css') {
            print_r($bundle->files);
            
            // File path of final result
            $filepath = DOCUMENT_ROOT . $stat->fileOutPath;

            $out = fopen($filepath, "w");
            
            // Then cycle through the files reading and writing.
            foreach ($bundle->files->param as $file) {

                $ext = explode('.', $file);
                echo $ext[1];
                if ($ext[1] == 'less') {

                    $parser = new Less_Parser();
                    $parser->parseFile(DOCUMENT_ROOT . $file);
                    $css = $parser->getCss();

                    fwrite($out, '/* --- begin file: ' . $file . ' --- */');
                    fwrite($out, $css);
                    fwrite($out, '/* --- end file: ' . $file . ' --- */');
                } else {
                    $in = fopen(DOCUMENT_ROOT . $file, "r");
                    while ($line = fgets($in)) {
                        fwrite($out, '/* --- begin file: ' . $file . ' --- */');
                        fwrite($out, $line);
                    }
                    fwrite($out, '/* --- end file: ' . $file . ' --- */');
                    fclose($in);
                }
            }
            // Then clean up
            fclose($out);
        }
    }
    
    $tpl = '';
    $types = array_keys($stats);
    $tpl .= '<ul>';
    foreach ($sizes as $type => $total) {
        $tpl .= "<li><a href='#$type'>" . strtoupper($type) . " Bundles</a> <span class='caption'>($total kb Total)</span></li>";
    }
    $tpl .= '</ul><br/>';
    
    foreach ($stats as $type => $bundleStats) {
        $tpl .= "<h2 id='$type'>" . strtoupper($type) . " Bundles</h2><br/>";
        $totalTypeSize = 0;
        foreach ($bundleStats as $stat) {
            // TODO Real size-limit
            $isTooBig = ($stat->totalSize > 1024) ? 'YES' : 'NO';
            $health = 'good';
            if ($stat->totalSize >= 2048) {
                $health = 'poor';
            } else if ($stat->totalSize >= 1024) {
                $health = 'fair';
            }
            $totalTypeSize += $stat->totalSize;
            $tpl .= "<table class='minifyResult' data-health='$health' data-active='on'>\n";
            $tpl .= "<thead>";
            $tpl .= "<th width='200px'>Total Size</th>";
            $tpl .= "<th>File</th>";
            $tpl .= "</thead>\n";
            $tpl .= "<tbody>\n";
            $tpl .= "<tr data-too-big='$isTooBig' data-health='$health'>";
            $tpl .= "<td>$stat->totalSize kb</td>";
            $tpl .= "<td>$stat->fileOutPath</td>";
            $tpl .= "</tr>";
            $tpl .= "<tr class='subhead'>";
            $tpl .= "<td>Minify? <span class='singleToggle'>Click to Toggle</span></td></td>";
            $tpl .= "<td>File(s) <span class='singleToggle'>Click to Toggle</span></td>";
            $tpl .= "</tr>";
            foreach ($stat->files as $file) {
                $minify = ($file['concatonly'] != 'true') ? 'YES' : 'NO';
                $tpl .= "<tr class='nested'>";
                $tpl .= "<td data-minify='$minify'>$minify</td>";
                $tpl .= "<td>$file</td>";
                $tpl .= "</tr>";
            }
            $tpl .= "</tbody>";
            $tpl .= "</table>";
        }
    }
    
    $dir = dirname($reportOut);    
    if (!file_exists($dir)) {
        echo 'Creating Minify Report Directory...';
        mkdir($dir, 0777, true);
    }

    $fh = fopen($reportOut, "w+");
    fwrite($fh, $layout);
    fclose($fh);
    echo "Done reporting on Minify...\n";
}
minifyReport();